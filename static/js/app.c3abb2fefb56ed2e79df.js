webpackJsonp([2,0],[
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _vue = __webpack_require__(32);
	
	var _vue2 = _interopRequireDefault(_vue);
	
	var _App = __webpack_require__(149);
	
	var _App2 = _interopRequireDefault(_App);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	new _vue2.default({
	  el: '#app',
	  template: '<App/>',
	  components: { App: _App2.default }
	});

/***/ },
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vue = __webpack_require__(32);
	
	var _vue2 = _interopRequireDefault(_vue);
	
	var _data_enGen = __webpack_require__(148);
	
	var _data_enGen2 = _interopRequireDefault(_data_enGen);
	
	var _dataGen = __webpack_require__(147);
	
	var _dataGen2 = _interopRequireDefault(_dataGen);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  get locale() {
	    return window.navigator.language;
	  },
	  set_locale: function set_locale(lang, reload) {
	    console.log(lang);
	    localStorage.setItem('lang', lang);
	    if (reload) {
	      location.reload();
	    }
	  },
	  get_default_locale: function get_default_locale() {
	    var lang = localStorage.getItem('lang');
	    console.log(lang);
	    if (!lang) {
	      return this.locale;
	    } else {
	      return lang;
	    }
	  },
	  get_locale: function get_locale() {
	    console.log('return ' + _vue2.default.config.lang);
	    return _vue2.default.config.lang;
	  },
	  get_data: function get_data(appMode) {
	    if (appMode) {
	      return _dataGen2.default;
	    } else {
	      var locale = this.get_default_locale();
	      if (locale.indexOf('zh') >= 0) {
	        return _dataGen2.default;
	      } else {
	        return _data_enGen2.default;
	      }
	    }
	  }
	};

/***/ },
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _cryptoJs = __webpack_require__(108);
	
	var CryptoJS = _interopRequireWildcard(_cryptoJs);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	var ENCRYPT_KEY = '46BE211EC5341106E7500D1E940A5410A2338C5FFC076225';
	var ENCRYPT_IV = '35C600FE961AFB23FC1D0E197281CE15';
	ENCRYPT_KEY = CryptoJS.enc.Utf8.parse(ENCRYPT_KEY);
	ENCRYPT_IV = CryptoJS.enc.Utf8.parse(ENCRYPT_IV);
	
	function encrypt(value) {
	  var encrypted = CryptoJS.AES.encrypt(value, ENCRYPT_KEY, {
	    iv: ENCRYPT_IV,
	    mode: CryptoJS.mode.CBC,
	    padding: CryptoJS.pad.Pkcs7
	  });
	  return encrypted.toString();
	}
	
	function decrypt(encrypted) {
	  if (!encrypted) {
	    return encrypted;
	  } else {
	    var decrypted = CryptoJS.AES.decrypt(encrypted, ENCRYPT_KEY, {
	      iv: ENCRYPT_IV,
	      mode: CryptoJS.mode.CBC,
	      padding: CryptoJS.pad.Pkcs7
	    });
	    return CryptoJS.enc.Utf8.stringify(decrypted);
	  }
	}
	
	exports.default = {
	  setItem: function setItem(key, value) {
	    if (value === null) {
	      localStorage.removeItem(key);
	    } else {
	      var result = encrypt(value);
	      localStorage.setItem(key, result);
	    }
	  },
	  getItem: function getItem(key) {
	    var value = void 0;
	
	    try {
	      value = localStorage.getItem(key);
	    } catch (e) {
	      console.log(e);
	    }
	
	    if (value) {
	      try {
	        return decrypt(value);
	      } catch (e) {
	        console.log(e);
	        return null;
	      }
	    } else {
	      return null;
	    }
	  }
	};

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _Header = __webpack_require__(156);
	
	var _Header2 = _interopRequireDefault(_Header);
	
	var _Footer = __webpack_require__(154);
	
	var _Footer2 = _interopRequireDefault(_Footer);
	
	var _HomeBlock = __webpack_require__(157);
	
	var _HomeBlock2 = _interopRequireDefault(_HomeBlock);
	
	var _SimpleBlock = __webpack_require__(162);
	
	var _SimpleBlock2 = _interopRequireDefault(_SimpleBlock);
	
	var _Exhibitions = __webpack_require__(153);
	
	var _Exhibitions2 = _interopRequireDefault(_Exhibitions);
	
	var _GameBlock = __webpack_require__(155);
	
	var _GameBlock2 = _interopRequireDefault(_GameBlock);
	
	var _MapBlock = __webpack_require__(159);
	
	var _MapBlock2 = _interopRequireDefault(_MapBlock);
	
	var _VoiceGuide = __webpack_require__(164);
	
	var _VoiceGuide2 = _interopRequireDefault(_VoiceGuide);
	
	var _AppLogin = __webpack_require__(152);
	
	var _AppLogin2 = _interopRequireDefault(_AppLogin);
	
	var _AboutApp = __webpack_require__(150);
	
	var _AboutApp2 = _interopRequireDefault(_AboutApp);
	
	var _TrafficsBlock = __webpack_require__(163);
	
	var _TrafficsBlock2 = _interopRequireDefault(_TrafficsBlock);
	
	var _AboutBlock = __webpack_require__(151);
	
	var _AboutBlock2 = _interopRequireDefault(_AboutBlock);
	
	var _LearningBlock = __webpack_require__(158);
	
	var _LearningBlock2 = _interopRequireDefault(_LearningBlock);
	
	var _QuizBlock = __webpack_require__(161);
	
	var _QuizBlock2 = _interopRequireDefault(_QuizBlock);
	
	var _ModalTemplate = __webpack_require__(160);
	
	var _ModalTemplate2 = _interopRequireDefault(_ModalTemplate);
	
	var _maintext = __webpack_require__(165);
	
	var _maintext2 = _interopRequireDefault(_maintext);
	
	var _config = __webpack_require__(14);
	
	var _config2 = _interopRequireDefault(_config);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var googleAnalytics = __webpack_require__(129);
	
	var BlockIdComponentMapping = {
	  'area': 'Exhibitions',
	  'learn': 'LearningBlock',
	  'map': 'MapBlock',
	  'voiceguide': 'VoiceGuide',
	  'transportation': 'TrafficsBlock',
	  'about': 'AboutBlock',
	  'game': 'GameBlock'
	};
	
	var IsExclusiveBlockIdDict = {
	  'applogin': true,
	  'aboutapp': true,
	  'voiceguide': true
	};
	
	exports.default = {
	  name: 'app',
	  components: {
	    HeaderPart: _Header2.default,
	    FooterPart: _Footer2.default,
	    HomeBlock: _HomeBlock2.default,
	    VoiceGuide: _VoiceGuide2.default,
	    AppLogin: _AppLogin2.default,
	    AboutApp: _AboutApp2.default,
	    SimpleBlock: _SimpleBlock2.default,
	    Exhibitions: _Exhibitions2.default,
	    GameBlock: _GameBlock2.default,
	    MapBlock: _MapBlock2.default,
	    TrafficsBlock: _TrafficsBlock2.default,
	    AboutBlock: _AboutBlock2.default,
	    LearningBlock: _LearningBlock2.default,
	    QuizBlock: _QuizBlock2.default,
	    ModalTemplate: _ModalTemplate2.default,
	    MainText: _maintext2.default
	  },
	  created: function created() {
	    var _this = this;
	
	    this.hash = window.location.hash ? window.location.hash.replace('#', '') : null;
	    window.addEventListener('hashchange', function () {
	      _this.hash = window.location.hash ? window.location.hash.replace('#', '') : null;
	
	      if (_this.hash) {
	        window.ga('send', {
	          hitType: 'event',
	          eventCategory: 'Browse',
	          eventAction: 'Navigate',
	          eventLabel: _this.hash
	        });
	      }
	    });
	    this.data = _config2.default.get_data(this.appMode);
	    this.analyticsInit();
	  },
	
	  computed: {
	    appMode: function appMode() {
	      return window.location.search.indexOf('app=true') >= 0;
	    },
	    defaultVoiceGuideID: function defaultVoiceGuideID() {
	      var search = window.location.search;
	      if (search) {
	        search = search.substr(1);
	        var targetSegment = search.split('&').find(function (segment) {
	          return segment.split('=')[0] === 'voiceGuideID';
	        });
	        return targetSegment ? targetSegment.split('=')[1] || '' : '';
	      } else {
	        return '';
	      }
	    },
	    isZh: function isZh() {
	      return _config2.default.get_default_locale().indexOf('zh') >= 0;
	    },
	    isExclusiveMode: function isExclusiveMode() {
	      return this.isExclusiveBlockId(this.hash);
	    },
	    isBlockVisible: function isBlockVisible() {
	      var _this2 = this;
	
	      return this.data.blocks.concat([{
	        id: 'home'
	      }, {
	        id: 'applogin'
	      }, {
	        id: 'aboutapp'
	      }]).reduce(function (result, block) {
	        if (_this2.isExclusiveMode) {
	          result[block.id] = block.id === _this2.hash;
	        } else if (_this2.appMode && _this2.hash != null) {
	          result[block.id] = block.id === _this2.hash;
	        } else if (_this2.isExclusiveBlockId(block.id)) {
	          result[block.id] = false;
	        } else if (block.id === 'quiz') {
	          result.quiz = _this2.isZh;
	        } else {
	          result[block.id] = true;
	        }
	        return result;
	      }, {});
	    },
	    isHeaderVisible: function isHeaderVisible() {
	      if (this.appMode) {
	        return false;
	      } else {
	        return !this.isExclusiveMode;
	      }
	    },
	    isFooterVisible: function isFooterVisible() {
	      return this.isHeaderVisible;
	    }
	  },
	  methods: {
	    isExclusiveBlockId: function isExclusiveBlockId(id) {
	      return !!IsExclusiveBlockIdDict[id];
	    },
	    getTitle: function getTitle(id) {
	      var i = void 0;
	      for (i = 0; i < this.data.blocks.length; i += 1) {
	        if (this.data.blocks[i].id === id) {
	          return this.data.blocks[i].title;
	        }
	      }
	      return 'none';
	    },
	    analyticsInit: function analyticsInit() {
	      var _this3 = this;
	
	      if (this.appMode) {
	        googleAnalytics.init('UA-96238039-2', this.appMode);
	      } else {
	        googleAnalytics.init('UA-96238039-1', this.appMode);
	      }
	
	      window.ga('send', 'pageview');
	      $(window).scroll(function () {
	        var at = window.location.hash;
	        if (at !== null && at !== undefined && _this3.currentPage !== at) {
	          _this3.currentPage = at;
	          window.ga('send', {
	            hitType: 'event',
	            eventCategory: 'Browse',
	            eventAction: 'Scroll',
	            eventLabel: at
	          });
	        }
	      });
	    }
	  },
	  filters: {
	    blockIdToComponent: function blockIdToComponent(value) {
	      return BlockIdComponentMapping[value];
	    }
	  },
	  data: function data() {
	    return {
	      hash: null,
	      data: null,
	      currentPage: '#home'
	    };
	  }
	};

/***/ },
/* 50 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'AboutApp',
	  props: ['data'],
	  data: function data() {
	    return {};
	  },
	
	  computed: {
	    emailLink: function emailLink() {
	      return this.data.strings.ABOUT_APP_EMAIL;
	    }
	  }
	};

/***/ },
/* 51 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'AboutBlock',
	  props: ['title', 'data'],
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _stringify = __webpack_require__(66);
	
	var _stringify2 = _interopRequireDefault(_stringify);
	
	var _assign = __webpack_require__(67);
	
	var _assign2 = _interopRequireDefault(_assign);
	
	var _vue = __webpack_require__(32);
	
	var _vue2 = _interopRequireDefault(_vue);
	
	var _storeService = __webpack_require__(48);
	
	var _storeService2 = _interopRequireDefault(_storeService);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var uuidSource = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var EmailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	
	exports.default = {
	  name: 'AppLogin',
	  props: ['data'],
	  data: function data() {
	    var _this = this;
	
	    var data = {
	      name: '',
	      email: '',
	      gender: null,
	      age: null,
	      loggedIn: false,
	      _QRCodeGenerator: null
	    };
	
	    var loginDataString = _storeService2.default.getItem('login_data');
	    var loginData = null;
	
	    if (loginDataString) {
	      loginData = JSON.parse(loginDataString);
	      (0, _assign2.default)(data, loginData);
	      data.name = decodeURI(data.name);
	      data.loggedIn = true;
	
	      _vue2.default.nextTick(function () {
	        _this.QRCodeGenerator.makeCode(loginDataString);
	      });
	    }
	
	    return data;
	  },
	
	  computed: {
	    QRCodeGenerator: function QRCodeGenerator() {
	      if (this._QRCodeGenerator == null) {
	        var style = window.getComputedStyle(this.$refs['page-container']);
	        var size = parseFloat(style.width) - 60;
	
	        this._QRCodeGenerator = new QRCode(this.$refs['qr-code-container'], {
	          text: 'qrcode',
	          width: size,
	          height: size
	        });
	      }
	      return this._QRCodeGenerator;
	    }
	  },
	  methods: {
	    validateData: function validateData() {
	      if (!this.name) {
	        if (window.ReactNativeWebView) {
	          window.ReactNativeWebView.postMessage(this.data.strings.APPLOGIN_ERRORMSG_NAME);
	        }
	        return false;
	      }
	      if (!this.email || !EmailRegex.test(this.email)) {
	        if (window.ReactNativeWebView) {
	          window.ReactNativeWebView.postMessage(this.data.strings.APPLOGIN_ERRORMSG_EMAIL);
	        }
	        return false;
	      }
	      if (!this.gender) {
	        if (window.ReactNativeWebView) {
	          window.ReactNativeWebView.postMessage(this.data.strings.APPLOGIN_ERRORMSG_GENDER);
	        }
	        return false;
	      }
	      if (!this.age) {
	        if (window.ReactNativeWebView) {
	          window.ReactNativeWebView.postMessage(this.data.strings.APPLOGIN_ERRORMSG_AGE);
	        }
	        return false;
	      }
	      return true;
	    },
	    generateUuid: function generateUuid(length) {
	      var result = [];
	      for (var i = 0; i < length; i += 1) {
	        result.push(uuidSource[Math.floor(Math.random() * uuidSource.length)]);
	      }
	      return result.join('');
	    },
	    login: function login() {
	      if (!this.validateData()) {
	        return;
	      }
	
	      var loginData = {
	        uuid: this.generateUuid(8),
	        name: encodeURI(this.name),
	        email: this.email,
	        gender: this.gender,
	        age: this.age,
	        type: 'APP'
	      };
	      var loginDataString = (0, _stringify2.default)(loginData);
	      _storeService2.default.setItem('login_data', loginDataString);
	
	      this.loggedIn = true;
	      this.QRCodeGenerator.makeCode(loginDataString);
	    },
	    logout: function logout() {
	      this.loggedIn = false;
	      this.name = '';
	      this.email = '';
	      this.gender = null;
	      this.age = null;
	      _storeService2.default.setItem('login_data', null);
	    }
	  }
	};

/***/ },
/* 53 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'Exhibitions',
	  props: ['data', 'title', 'appMode'],
	  data: function data() {
	    return {
	      current_idx: 0,
	      desktopMode: true
	    };
	  },
	  created: function created() {
	    var _this = this;
	
	    $(window).resize(function () {
	      _this.updateMode();
	    });
	    this.updateMode();
	  },
	  mounted: function mounted() {
	    $('#areaCarousel').carousel({
	      interval: false
	    });
	  },
	
	  methods: {
	    modal: function modal(item) {
	      this.$root.$emit('show_modal', item);
	    },
	    get_items: function get_items(id) {
	      var ret = [];
	      this.data.items.forEach(function (item) {
	        if (item.id === id) {
	          ret.push(item);
	        }
	      });
	      return ret;
	    },
	    choose: function choose(value) {
	      this.current_idx = value;
	    },
	    next: function next() {
	      this.current_idx = ($('div#areaCarousel .carousel-indicators > li.active').index() + 1) % this.data.exhibitions.length;
	      $('#areaCarousel').carousel(this.current_idx);
	    },
	    prev: function prev() {
	      this.current_idx = $('div#areaCarousel .carousel-indicators > li.active').index() - 1;
	      if (this.current_idx < 0) {
	        this.current_idx = this.data.exhibitions.length - 1;
	      }
	      $('#areaCarousel').carousel(this.current_idx);
	    },
	    updateMode: function updateMode() {
	      if (this.appMode || $(window).width() < 1024 || $(window).height() < 768) {
	        this.desktopMode = false;
	      } else {
	        this.desktopMode = true;
	      }
	    }
	  }
	};

/***/ },
/* 54 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'footer',
	  props: ['data'],
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 55 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'GameBlock',
	  props: ['data', 'title', 'appMode'],
	  data: function data() {
	    return {
	      current_idx: 0,
	      desktopMode: true
	    };
	  },
	  created: function created() {
	    var _this = this;
	
	    $(window).resize(function () {
	      _this.updateMode();
	    });
	    this.updateMode();
	  },
	  mounted: function mounted() {
	    $('#gameCarousel').carousel({
	      interval: false
	    });
	  },
	
	  methods: {
	    modal: function modal(item) {
	      this.$root.$emit('show_modal', item);
	    },
	    get_items: function get_items(id) {
	      var ret = [];
	      this.data.items.forEach(function (item) {
	        if (item.id === id) {
	          ret.push(item);
	        }
	      });
	      return ret;
	    },
	    choose: function choose(value) {
	      this.current_idx = value;
	    },
	    next: function next() {
	      this.current_idx = ($('div#gameCarousel .carousel-indicators > li.active').index() + 1) % this.data.exhibitions.length;
	      $('#gameCarousel').carousel(this.current_idx);
	    },
	    prev: function prev() {
	      this.current_idx = $('div#gameCarousel .carousel-indicators > li.active').index() - 1;
	      if (this.current_idx < 0) {
	        this.current_idx = this.data.exhibitions.length - 1;
	      }
	      $('#gameCarousel').carousel(this.current_idx);
	    },
	    updateMode: function updateMode() {
	      if (this.appMode || $(window).width() < 1024 || $(window).height() < 768) {
	        this.desktopMode = false;
	      } else {
	        this.desktopMode = true;
	      }
	    }
	  }
	};

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _config = __webpack_require__(14);
	
	var _config2 = _interopRequireDefault(_config);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  name: 'simple',
	  computed: {
	    isZh: function isZh() {
	      return _config2.default.get_default_locale().indexOf('zh') >= 0;
	    },
	    menus: function menus() {
	      var _this = this;
	
	      return this.data.menus.filter(function (menu) {
	        if (menu.link === '#quiz') {
	          return _this.isZh;
	        } else {
	          return true;
	        }
	      });
	    }
	  },
	  methods: {
	    toggleLocale: function toggleLocale() {
	      var lang = 'en';
	      if (_config2.default.get_default_locale() === 'en') {
	        lang = 'zh';
	      }
	      _config2.default.set_locale(lang, true);
	      this.analytics(lang);
	    },
	    analytics: function analytics(id) {
	      window.ga('send', {
	        hitType: 'event',
	        eventCategory: 'Browse',
	        eventAction: 'Click',
	        eventLabel: id
	      });
	    }
	  },
	  data: function data() {
	    return {
	      data: _config2.default.get_data()
	    };
	  }
	};

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _config = __webpack_require__(14);
	
	var _config2 = _interopRequireDefault(_config);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  name: 'HomeBlock',
	  data: function data() {
	    return {
	      banner_url: 'static/img/bg-banner.jpg',
	      data: _config2.default.get_data()
	    };
	  },
	
	  methods: {
	    analytics: function analytics(appName) {
	      window.ga('send', {
	        hitType: 'event',
	        eventCategory: 'APP Button',
	        eventAction: 'Click',
	        eventLabel: appName
	      });
	    }
	  }
	};

/***/ },
/* 58 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var LearningSheetsAPI = 'https://physworld.nmns.edu.tw/api/learningsheets';
	
	exports.default = {
	  name: 'LearningBlock',
	  props: ['data', 'title'],
	  data: function data() {
	    return {
	      appMode: false,
	      learningSheets: [{
	        title: this.data.strings.LEARNING_TABLE_EASY,
	        category: this.data.strings.LEARNING_EASY,
	        file: 'https://physworld.nmns.edu.tw/static/learn-sheet/learn-sheet-easy.pdf'
	      }, {
	        title: this.data.strings.LEARNING_TABLE_MEDIUM,
	        category: this.data.strings.LEARNING_MEDIUM,
	        file: 'https://physworld.nmns.edu.tw/static/learn-sheet/learn-sheet-medium.pdf'
	      }, {
	        title: this.data.strings.LEARNING_TABLE_HARD,
	        category: this.data.strings.LEARNING_HARD,
	        file: 'https://physworld.nmns.edu.tw/static/learn-sheet/learn-sheet-difficult.pdf'
	      }]
	    };
	  },
	  mounted: function mounted() {
	    var _this = this;
	
	    this.appMode = !!document.querySelector('[app-mode]');
	
	    $.getJSON(LearningSheetsAPI, function () {}).done(function (result) {
	      if (!_this.appMode) {
	        _this.learningSheets = result.data;
	      }
	    });
	  },
	
	  methods: {
	    processDownloadUrl: function processDownloadUrl(item) {
	      if (item.id !== undefined) {
	        return 'https://physworld.nmns.edu.tw/learningsheets/' + item.id + '/download';
	      } else {
	        return item.file;
	      }
	    },
	    analytics: function analytics(filename) {
	      window.ga('send', {
	        hitType: 'event',
	        eventCategory: 'Download',
	        eventAction: 'Click',
	        eventLabel: filename
	      });
	    }
	  }
	};

/***/ },
/* 59 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'MapBlock',
	  props: ['title', 'data'],
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 60 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'ModalTemplate',
	  props: ['data'],
	  data: function data() {
	    return {
	      content: {
	        title: 'title'
	      }
	    };
	  },
	  created: function created() {
	    var _this = this;
	
	    this.$root.$on('show_modal', function (content) {
	      console.log(content);
	      _this.content = content;
	
	      $('#myModal').modal('show');
	    });
	  },
	
	  computed: {
	    appMode: function appMode() {
	      return window.location.search.indexOf('app=true') >= 0;
	    },
	    hasVoiceGuide: function hasVoiceGuide() {
	      var _this2 = this;
	
	      if (!this.content.iid) {
	        return false;
	      }
	
	      if (this.content.iid.indexOf('1-') >= 0) {
	        return false;
	      }
	
	      return !!this.data.voiceguide.find(function (item) {
	        return item.id === _this2.content.iid;
	      }, this);
	    }
	  },
	  methods: {
	    playVoiceGuide: function playVoiceGuide() {
	      if (this.hasVoiceGuide) {
	        var url = 'voiceGuideID=' + this.content.iid + '#voiceguide';
	        if (this.appMode) {
	          window.location.assign('?app=true&mode=standalone&' + url);
	        } else {
	          window.location.assign('?' + url);
	        }
	      }
	    }
	  }
	};

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _promise = __webpack_require__(68);
	
	var _promise2 = _interopRequireDefault(_promise);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var DefaultQuestions = [{
	  q: '水管接龍中水要填滿的容積/面積越大，需要越多的時間填滿。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '流體力學是一種液體的力學，不包含氣體以及電漿體。',
	  a: 2,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '光進入一個介質後又回到原來的介質，例如：把鏡子對太陽光，這個現象叫做反射。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '太陽光如果從空中照進平靜的湖面，會同時發生反射與折射的現象。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '物體(或系統)運動狀態保持不變的情況下，能量可以變為不同的形式，被稱為動量守恆。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '當火箭升空時，獲得向天空的動能大於火箭尾部往地面噴發的氣體能量。',
	  a: 2,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '由於引力的不同，在地球上原地跳30公分高的人，在月球上可以跳到180公分高。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '質量越大的物體，所產生的引力也越大，所以太陽對地球的引力大於地球對太陽的引力，但因為地球繞行太陽，產生一股與太陽引力相互平衡的離心力，讓地球不至於被太陽的引力拉過去。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '摩擦力會將物體的熱能，轉換成為動能。',
	  a: 2,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '用力推酒杯時，酒杯在平滑的桌面上滑行的距離會比在粗糙的桌面上滑行的遠，是因為平滑桌面的摩擦係數較小。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '蛇擺是由數個單擺組成，單擺會因為擺長的不同長度而產生不同週期的擺動。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '蛇擺一開始會出現行進波的形狀，如同蛇的型態，故稱為蛇擺。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '當磁體分為N、S兩極，當磁體N極靠近另一塊磁體的N極時，兩塊磁體會吸在一起。',
	  a: 2,
	  a1: '是',
	  a2: '否'
	}, {
	  q: '馬達(電動機)被廣泛運用於各種電器用品間，能將電能轉換為機械能，其原理就是透過電磁鐵，利用電流通過而產生磁力。',
	  a: 1,
	  a1: '是',
	  a2: '否'
	}];
	
	var QuestionsAPI = 'https://physworld.nmns.edu.tw/api/questions';
	
	exports.default = {
	  name: 'LearningBlock',
	  props: ['data', 'title'],
	  data: function data() {
	    return {
	      appMode: false,
	      targetQuestion: null,
	      isDialogVisible: false,
	      dialogText: '',
	      dialogDefer: null,
	      questions: DefaultQuestions
	    };
	  },
	  mounted: function mounted() {
	    var _this = this;
	
	    this.appMode = !!document.querySelector('[app-mode]');
	
	    $.getJSON(QuestionsAPI, function () {}).done(function (result) {
	      _this.questions = result.data;
	    }).always(function () {
	      _this.targetQuestion = _this.getRandomQuestion();
	    });
	  },
	
	  methods: {
	    processDownloadUrl: function processDownloadUrl(url) {
	      if (this.appMode) {
	        return 'https://physworld.nmns.edu.tw/' + url;
	      } else {
	        return url;
	      }
	    },
	    analytics: function analytics(filename) {
	      window.ga('send', {
	        hitType: 'event',
	        eventCategory: 'Download',
	        eventAction: 'Click',
	        eventLabel: filename
	      });
	    },
	    getRandomQuestion: function getRandomQuestion() {
	      var index = Math.floor(Math.random() * 1000000 % this.questions.length);
	      return this.questions[index];
	    },
	    answer: function answer(answerIndex) {
	      var _this2 = this;
	
	      if (answerIndex == this.targetQuestion.a) {
	        this.showDialog(this.l10n('QUIZ_CORRECT_MSG')).then(function () {
	          _this2.targetQuestion = _this2.getRandomQuestion();
	        });
	      } else {
	        this.showDialog(this.l10n('QUIZ_INCORRECT_MSG')).then(function () {
	          _this2.targetQuestion = _this2.getRandomQuestion();
	        });
	      }
	    },
	    showDialog: function showDialog(text) {
	      var _this3 = this;
	
	      this.isDialogVisible = true;
	      this.dialogText = text;
	      this.dialogDefer = {};
	      var promise = new _promise2.default(function (resolve) {
	        _this3.dialogDefer.resolve = resolve;
	      });
	      this.dialogDefer.promise = promise;
	      return promise;
	    },
	    hideDialog: function hideDialog() {
	      this.isDialogVisible = false;
	      this.dialogText = '';
	      if (this.dialogDefer) {
	        this.dialogDefer.resolve();
	      }
	    },
	    l10n: function l10n(id) {
	      return this.data.strings[id];
	    }
	  }
	};

/***/ },
/* 62 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'SimpleBlock',
	  props: ['data'],
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 63 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'TrafficsBlock',
	  props: ['title', 'data'],
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _config = __webpack_require__(14);
	
	var _config2 = _interopRequireDefault(_config);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var DocumentHiddenString = '';
	
	exports.default = {
	  name: 'VoiceGuide',
	  props: ['data', 'defaultVoiceGuideID'],
	  data: function data() {
	    return {
	      appMode: false,
	      playMode: false,
	      isPlay: false,
	      playButtonText: 'Pause',
	      isShowError: false,
	      targetVoiceGuideTrack: null,
	      audioProgress: 0,
	      voiceGuideID: ''
	    };
	  },
	
	  computed: {
	    exitText: function exitText() {
	      if (this.isStandaloneMode) {
	        return this.data.strings.VOICE_PLAY_CLOSE_BTN;
	      } else {
	        return this.data.strings.VOICE_PLAY_BACK_BTN;
	      }
	    },
	    isStandaloneMode: function isStandaloneMode() {
	      return window.location.search.indexOf('mode=standalone') >= 0;
	    }
	  },
	  mounted: function mounted() {
	    this.appMode = !!document.querySelector('[app-mode]');
	    if (this.defaultVoiceGuideID !== null && this.defaultVoiceGuideID !== undefined && this.defaultVoiceGuideID !== '') {
	      this.voiceGuideID = this.defaultVoiceGuideID;
	      this.targetVoiceGuideTrack = this.lookup(this.voiceGuideID);
	
	      if (this.targetVoiceGuideTrack) {
	        this.play();
	      } else if (this.appMode) {
	        if (window.ReactNativeWebView) {
	          window.ReactNativeWebView.postMessage('no-voice-guide-found');
	        }
	      }
	    }
	    if (this.appMode) {
	      this.registerVisibilityChangeHandler();
	    }
	  },
	
	  methods: {
	    registerVisibilityChangeHandler: function registerVisibilityChangeHandler() {
	      var visibilityChange = '';
	      if (typeof document.hidden !== 'undefined') {
	        DocumentHiddenString = 'hidden';
	        visibilityChange = 'visibilitychange';
	      } else if (typeof document.msHidden !== 'undefined') {
	        DocumentHiddenString = 'msHidden';
	        visibilityChange = 'msvisibilitychange';
	      } else if (typeof document.webkitHidden !== 'undefined') {
	        DocumentHiddenString = 'webkitHidden';
	        visibilityChange = 'webkitvisibilitychange';
	      }
	      if (visibilityChange) {
	        document.addEventListener(visibilityChange, this.handleVisibilitychange);
	      }
	    },
	    handleVisibilitychange: function handleVisibilitychange() {
	      if (document[DocumentHiddenString]) {
	        this.pause();
	      }
	    },
	    lookup: function lookup(voiceGuideID) {
	      var _this = this;
	
	      var ret = null;
	      this.data.voiceguide.forEach(function (item) {
	        if (item.id === voiceGuideID) {
	          ret = item;
	          _this.analytics(ret.id);
	        }
	      }, this);
	      return ret;
	    },
	    togglePlay: function togglePlay() {
	      var a = this.$refs.audioPlayer;
	      if (a) {
	        if (a.paused) {
	          a.play();
	          this.isPlay = true;
	          this.playButtonText = 'Pause';
	        } else {
	          this.pause();
	        }
	      }
	    },
	    press: function press(n) {
	      this.voiceGuideID += n.toString();
	    },
	    showError: function showError() {
	      var _this2 = this;
	
	      this.isShowError = true;
	      setTimeout(function () {
	        _this2.isShowError = false;
	        _this2.voiceGuideID = '';
	      }, 1000);
	    },
	    targetVoicePath: function targetVoicePath() {
	      var lang = _config2.default.get_default_locale();
	      if (lang.indexOf('zh') < 0) {
	        lang = 'en';
	      } else {
	        lang = 'zh';
	      }
	      if (this.appMode) {
	        return 'https://physworld.nmns.edu.tw/static/voice/' + lang + '/' + this.targetVoiceGuideTrack.id + '.mp3';
	      } else {
	        return 'static/voice/' + lang + '/' + this.targetVoiceGuideTrack.id + '.mp3';
	      }
	    },
	    confirm: function confirm() {
	      var item = this.lookup(this.voiceGuideID);
	      if (item != null && this.voiceGuideID.substring(0, 2) !== '1-') {
	        this.targetVoiceGuideTrack = item;
	        this.play();
	      } else {
	        this.showError();
	      }
	    },
	    play: function play() {
	      var _this3 = this;
	
	      this.playMode = true;
	      setTimeout(function () {
	        var a = _this3.$refs.audioPlayer;
	        a.play();
	        _this3.isPlay = true;
	        a.onended = function () {
	          _this3.audioProgress = 0;
	          _this3.playButtonText = 'Play';
	          _this3.isPlay = false;
	        };
	        a.onerror = function () {
	          _this3.isPlay = false;
	        };
	        a.ontimeupdate = function () {
	          a = _this3.$refs.audioPlayer;
	          if (a) {
	            _this3.audioProgress = a.currentTime * 100 / a.duration;
	          } else {
	            _this3.audioProgress = 0;
	          }
	        };
	      }, 100);
	    },
	    pause: function pause() {
	      var a = this.$refs.audioPlayer;
	      if (a) {
	        a.pause();
	        this.isPlay = false;
	        this.playButtonText = 'Play';
	      }
	    },
	    clear: function clear() {
	      this.isShowError = false;
	      this.voiceGuideID = '';
	    },
	    back: function back() {
	      if (this.isStandaloneMode) {
	        window.history.back();
	      } else if (this.appMode) {
	        this.voiceGuideID = '';
	        if (window.ReactNativeWebView) {
	          window.ReactNativeWebView.postMessage('voice-guide-request-back');
	        }
	      } else if (this.playMode) {
	        this.playMode = false;
	        this.voiceGuideID = '';
	      } else {
	        window.location = '#home';
	      }
	      this.pause();
	    },
	    analytics: function analytics(id) {
	      window.ga('send', {
	        hitType: 'event',
	        eventCategory: 'Voice',
	        eventAction: 'Play',
	        eventLabel: id
	      });
	    }
	  }
	};

/***/ },
/* 65 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'simple',
	  props: ['data'],
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */
/***/ function(module, exports) {

	module.exports = {
	  init(id, isApp) {
	    window.ga = window.ga || function () { (ga.q = ga.q || []).push(arguments); }; ga.l = +new Date();
	    if (isApp) {
	      let uuid = localStorage.getItem('uuid');
	      if (!uuid) {
	        uuid = this.generateUUID();
	        localStorage.setItem('uuid', uuid);
	      }
	
	  		ga('create', id, {
	        'cookieDomain': 'none',
	        'storage': 'none',
	        'clientId': uuid
	      });
	
	      ga('set', 'checkProtocolTask', () => {});
	    } else {
	      ga('create', id, 'auto');
	    }
	    ga('send', 'pageview');
	  },
	
	  generateUUID() {
	    let d = new Date().getTime();
	    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
	      const r = (d + Math.random() * 16) % 16 | 0;
	      d = Math.floor(d / 16);
	      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	    });
	    return uuid;
	  }
	};


/***/ },
/* 130 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 131 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 132 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 133 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 134 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 135 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 136 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 137 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 138 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 139 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 140 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 141 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 142 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 143 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 144 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 145 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 146 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 147 */
/***/ function(module, exports) {

	module.exports = {
		"menus": [
			{
				"title": "首頁",
				"link": "#home"
			},
			{
				"title": "展區介紹",
				"link": "#area",
				"comments": "refers to exhibitions 八大展區 每一個都放一個carousel for items"
			},
			{
				"title": "八大遊戲",
				"link": "#game",
				"comments": "遊戲介紹"
			},
			{
				"title": "語音導覽",
				"link": "#voiceguide",
				"comments": "特別頁製作 輸入號碼 VOICE MP3"
			},
			{
				"title": "學習園地",
				"link": "#learn",
				"comments": "學習單列表 下載"
			},
			{
				"title": "體驗學習",
				"link": "#quiz"
			},
			{
				"title": "展區地圖",
				"link": "#map",
				"comments": "文字+圖"
			},
			{
				"title": "交通資訊",
				"link": "#transportation"
			},
			{
				"title": "參觀資訊",
				"link": "#about",
				"comments": "開放時間+相關連結+FB粉絲團+介紹影片"
			}
		],
		"blocks": [
			{
				"id": "area",
				"title": "展區介紹"
			},
			{
				"id": "game",
				"title": "八大遊戲"
			},
			{
				"id": "voiceguide",
				"title": "語音導覽"
			},
			{
				"id": "learn",
				"title": "學習園地"
			},
			{
				"id": "quiz",
				"title": "體驗學習"
			},
			{
				"id": "map",
				"title": "展區地圖"
			},
			{
				"id": "transportation",
				"title": "交通資訊"
			},
			{
				"id": "about",
				"title": "參觀資訊"
			}
		],
		"exhibitions": [
			{
				"id": "1",
				"title": "序展",
				"image": "static/img/exhibit/01.jpg"
			},
			{
				"id": "2",
				"title": "電磁學",
				"description": "電磁學是物理的五大領域之一，隸屬於古典物理的一支。電磁學就是描述電、磁以及電與磁交互作用的領域。靜電是描述電荷處在平衡狀態下，電力、電場、電位能與電位的關係。因為電荷是靜止狀態，所以應用到的力學的觀念主要是靜力平衡的合力與合力矩為零的概念以及包含力學能守恆與功能原理在內的能量觀念。電流描述的對象是穩定流動的電荷，因為它的穩定流動，所以需要電路來描述它。\n厄斯特無意間的發現，開啟了”電動生磁”的年代，使得磁學變成了大熱門。在實際生活的應用上各式電器把電能轉變成磁能，再進一步的輸出力學能，對於人類生活的提升有著重要的貢獻，這就是電動機的功能。 最後一個領域則是”電磁感應”，它不但能把生活中的力學能儲存、開發成電能，更進一步，將電能以電磁波方式傳遞出去。\n",
				"image": "static/img/exhibit/02.jpg"
			},
			{
				"id": "3",
				"title": "運動學",
				"description": "動力學是古典力學的一門分支，主要研究運動的變化與造成這變化的各種因素，純粹描述物體的運動，完全不考慮導致運動的因素。\n動力學的基礎定律是艾薩克·牛頓提出的牛頓運動定律：第一定律（慣性定律）、第二定律（加速度定律）、第三定律（作用力與反作用力定律），對於任意物理系統，只要知道其作用力的性質，引用牛頓運動定律，就可以研究這作用力對於這物理系統的影響。",
				"image": "static/img/exhibit/03.jpg"
			},
			{
				"id": "4",
				"title": "力學",
				"description": "力學是物理學的一個分支，主要研究能量和力以及它們與物體的平衡、變形或運動的關係。伽利略的自由落體運動規律，以及牛頓的運動定律皆奠定了動力學的基礎。力學主要可分為古典力學及量子力學，古典力學主要研究低速或靜止的宏觀物體，探討物質的運動和彼此之間的交互作用，小至質點，大至星系、宇宙；量子力學應用範圍較廣，主要是描述微觀之物（分子、原子、次原子粒子）的理論。\n",
				"image": "static/img/exhibit/04.jpg"
			},
			{
				"id": "5",
				"title": "流體力學",
				"description": "流體力學是力學的一門分支，是研究流體（包含氣體、液體及電漿體）現象以及相關力學行為的科學。流體力學可以按照研究對象的運動方式分為流體靜力學和流體動力學，前者研究處於靜止狀態的流體，後者研究力對於流體運動的影響。流體力學按照應用範圍，分為：空氣力學及水力學等等。",
				"image": "static/img/exhibit/05.jpg"
			},
			{
				"id": "6",
				"title": "波動",
				"description": "波動是空間上傳播的一種物理現象，波的傳播速度總是有限的。波動通常是一種藉由介質(群體粒子的行為)來傳遞能量的行為，例如聲波、水波；但有些波動則不需要介質傳遞能量，例如光波。波速與介質的彈性與慣性有關，但與波源的性質無關。波根據振動源的次數可以分為脈波與週期波。波在均勻、無向性的介質中傳遞時，依介質的振動方向分可以分為縱波與橫波。\n",
				"image": "static/img/exhibit/06.jpg"
			},
			{
				"id": "7",
				"title": "近代物理",
				"description": "二十世紀是物理學的大發現時代，也是物理概念的大革命時代。在這段時期所發展的物理學稱為近代物理學，而在這之前的物理學則稱為古典物理學。「X 光」、「天然放射線」、「電子」三項重大的物理發現，拉開了二十世紀物理學大發現時代的序幕。而量子力學和相對論構成了近代物理的兩塊重要基石。",
				"image": "static/img/exhibit/07.jpg"
			},
			{
				"id": "8",
				"title": "光學",
				"description": "光學主要是研究光的現象、性質與應用，包括光與物質之間的交互作用、光學儀器的製作。光學通常研究紅外線、紫外線及可見光的物理行為。因為光是電磁波，其它形式的電磁輻射，例如X射線、微波、電磁輻射及無線電波等等也具有類似光的特性。其中幾何光學，描述了光的傳播，一個由透鏡、反射鏡及稜鏡組合而成的光學系統，用幾何光學可以說明其中的反射、折射等現象。\n",
				"image": "static/img/exhibit/08.jpg"
			}
		],
		"items": [
			{
				"id": "1",
				"iid": "1-1",
				"category": "序展",
				"title": "物理之門",
				"title_en": "Physics Gate",
				"image": "static/img/exhibit/1-1.jpg",
				"video": "https://www.youtube.com/embed/Jj9p3Yjklvw"
			},
			{
				"id": "1",
				"iid": "1-2",
				"category": "序展",
				"title": "總部：物理小老師",
				"title_en": "Headquarter:Physics Tutor",
				"image": "static/img/exhibit/1-2.jpg"
			},
			{
				"id": "1",
				"iid": "1-3",
				"category": "序展",
				"title": "互動學習桌（後端學習系統）",
				"title_en": "Interactive Learning Desk (Back End LearningSystem)",
				"image": "static/img/exhibit/1-3.jpg"
			},
			{
				"id": "1",
				"iid": "1-5",
				"category": "序展",
				"title": "精神堡壘",
				"title_en": "Fortress of Spirit",
				"image": "static/img/exhibit/1-5.jpg"
			},
			{
				"id": "2",
				"iid": "2-1",
				"category": "電磁學",
				"title": "人造磁雲",
				"title_en": "Artificial Magnetic Cloud",
				"description": "握著磁鐵，從容器外面底部緩慢將鐵粉吸引上來，然後將磁鐵拉開，鐵粉就會飄舞而下，形成一片夢幻般的「磁雲」！再用磁鐵吸引尚未完全沈澱的鐵粉，鐵粉又會重新聚集，此時從對面看，會發現鐵粉依據特殊的線條向外排列延伸，這些線條是什麼？",
				"image": "static/img/exhibit/2-1.jpg"
			},
			{
				"id": "2",
				"iid": "2-2",
				"category": "電磁學",
				"title": "物質的磁性",
				"title_en": "Magnetic Property of Matter",
				"description": "輪流拿起不同的棒子，保持水平，從上往下通過磁場拱門，這個外加磁場會對哪一根棒子產生拉扯？產生拉扯時，是吸引力還是排斥力？為什麼？有拉扯感覺的棒子，是它自己本來就有磁力嗎？嘗試完了每一根棒子之後，比較不同材質的棒子受到拉扯的有無和強弱，以瞭解物質的磁性。在看過旁邊螢幕中的「科學介紹」後，想想看，磁場拱門裡裝的是什麼東西？具有何種磁性？",
				"image": "static/img/exhibit/2-2.jpg"
			},
			{
				"id": "2",
				"iid": "2-3",
				"category": "電磁學",
				"title": "若即若離的磁力",
				"title_en": "Magnetic Force - Attractive or Repulsive ",
				"description": "圓環中央的磁鐵和外面的磁鐵互相吸引，可是圓環周邊的6個小磁鐵卻和外面的磁鐵互相排斥，在這種組合下，將圓環靠近外面的磁鐵，會發生什麼事？會互相吸引還是互相排斥？靠近之後再拉遠，會發生什麼事？如果不是在同一平面上，而是在垂直方向上互相接近，又會出現何種現象？吸進來還是彈出去？如果將這個垂直裝置反過來呢？外加磁鐵會不會掉下去？",
				"image": "static/img/exhibit/2-3.jpg"
			},
			{
				"id": "2",
				"iid": "2-4",
				"category": "電磁學",
				"title": "違反能量守恆的碰撞？",
				"title_en": "Collision that Violates Energy Conservation？",
				"description": "按下按鈕，看小球從平台上緩緩滾下，撞向一串五個小球，這一串小球的最後一個球獲得撞擊的能量，竟然能跑到比原先小球更高的地方！這表示它獲得了更多的位能，但這不是違反了能量守恆的原理嗎？發揮你福爾摩斯的觀察能力，看看這個顯然違反能量守恆的過程，找出關鍵秘訣在哪裡？",
				"image": "static/img/exhibit/2-4.jpg"
			},
			{
				"id": "2",
				"iid": "2-5",
				"category": "電磁學",
				"title": "磁力抗拒重力？",
				"title_en": "Magnetic Force against Gravitational Force",
				"description": "拿起桌上的螺圈，吸引展品箱中的小球，將小球輪流帶到各個管子的上方，讓它掉入管中，觀察小球在不同管子中掉出所需的時間是否相同？如果不同，哪個管子所花的時間最短？哪根管子所花的時間最久？為什麼？如果你身材夠高，可以在放入小球後，從管子上方往裡面看，觀察小球緩慢但均勻下落的過程。",
				"image": "static/img/exhibit/2-5.jpg"
			},
			{
				"id": "2",
				"iid": "2-6",
				"category": "電磁學",
				"title": "看不見的吸引力",
				"title_en": "The Invisible Attractive Force",
				"description": "先按下按鈕，讓線圈通電，再輪流拿不同材質的棒子插入線圈中央，感受一下哪種材質的棒子會受到拉扯？是吸引力還是排斥力？為什麼？如果線圈沒有通電，會產生同樣的現象嗎？比較各個棒子的材質，就會知道不同物質的磁性表現。",
				"image": "static/img/exhibit/2-6.jpg"
			},
			{
				"id": "3",
				"iid": "3-1",
				"category": "運動學",
				"title": "永不停歇的風火輪",
				"title_en": "Long-Lasting Wheels of Wind and Fire",
				"description": "桌面中央是一個高速轉動的圓盤，將圓球從這一端滾向那一端，圓球會走出什麼軌跡？將圓球擺在圓盤的中心，當它逐漸離開中心，它的「公轉」和「自轉」會產生什麼變化？為什麼？試著將各個圓盤或圓環放上轉盤，看能不能找出一個讓這些物體持久轉動的方法？試著讓圓盤或圓環接觸轉盤時反向滾動，是否有幫助？",
				"image": "static/img/exhibit/3-1.jpg"
			},
			{
				"id": "3",
				"iid": "3-2",
				"category": "運動學",
				"title": "轉動慣量與轉動動能",
				"title_en": "Moment of Inertia and Rotational Energy",
				"description": "圓盤兩側各有把手，可以反向轉動調整重錘的位置，以改變整個圓盤的質量分布。把一個圓盤的重錘都集中到中心，另一個圓盤的重錘分散到最外緣，將兩個圓盤從滑道頂端同時放開，誰會先到終點？為什麼？",
				"image": "static/img/exhibit/3-2.jpg"
			},
			{
				"id": "3",
				"iid": "3-3",
				"category": "運動學",
				"title": "圓球‧圓盤‧圓環，誰跑得最快？",
				"title_en": "A Sphere, a Disc, and A Ring, which one runs the fastest?",
				"description": "圓球、圓盤，和圓環三者質量相同，從同一高度放開，讓它們滾動而下，誰會先到終點？為什麼？",
				"image": "static/img/exhibit/3-3.jpg"
			},
			{
				"id": "3",
				"iid": "3-4",
				"category": "運動學",
				"title": "滾球接龍",
				"title_en": "Rolling Ball Relay",
				"description": "將各種磁性軌道用不同方式排列在牆面上，形成一串向下滑動的路徑，看誰做的軌道能讓小球滾最久？這個展品顯示的是哪兩種能量之間的轉換？",
				"image": "static/img/exhibit/3-4.jpg"
			},
			{
				"id": "4",
				"iid": "4-1",
				"category": "力學",
				"title": "科氏力大型咖啡杯",
				"title_en": "Grandiose Coffee Cup of Coriolis Force",
				"description": "按照解說人員的指示，依序進入咖啡杯坐定，在咖啡杯開始轉動之前，先觀察地面小球直線滾動的軌跡，在咖啡杯達到穩定轉動之後，再觀察小球在地面滾動的路徑，和先前有無差異？若有，這個改變是真的還是假的？在轉動之前，解說人員讓中央垂降的「佛科擺」開始擺動，開始轉動之後，觀察擺動平面有無變化？若有，這個變化是真的還是假的？這些變化都是「科氏力」造成的，而這個「科氏力」是一個真實的力嗎？離開咖啡杯之後，到欄杆外面觀賞銀幕上的四分割畫面，看別人在體驗時，滾球路徑或單擺平面從「轉動座標系」和「靜止座標系」觀察的差異。",
				"image": "static/img/exhibit/4-1.jpg"
			},
			{
				"id": "4",
				"iid": "4-1",
				"title": "電力回生系統",
				"title_en": "Power Feedback System",
				"description": "本展品裝設台達提供的「電力回生系統」，以回收剎車能量再利用。以往的動力設備透過煞車電阻來消耗能量以達到減速目的，但這個過程除了浪費能量，也容易過熱，台達的系統會讓馬達在煞車過程中轉變為發電機，將煞車能量轉換成電能，傳輸回電力系統再利用，以達到節能環保的效果。",
				"image": "static/img/exhibit/4-1.jpg"
			},
			{
				"id": "4",
				"iid": "4-2",
				"category": "力學",
				"title": "忠實的牛頓擺",
				"title_en": "Faithful Newton's Cradle",
				"description": "牛頓擺看來簡單，就是五個小球彼此互撞，但它其實有很多種展示方法，可以測驗你對「能量」和「動量」的瞭解！一個小球拉起來撞回去，是撞飛一個球，還是撞飛四個球？為什麼？兩個球拉起來撞下去，會怎樣？到三個球和四個球，就真的需要思考了！另外從兩邊各拉起一個球，撞下去會不會彈回去？如果會彈起來，那是因為小球「撞牆」的緣故，還是因為左右小球彼此能量互換？要怎麼測試才會知道？右邊的五個球質量一樣，但是都較輕，左邊的五個球質量不同，在這些系統中，重複以上過程，會有什麼結果？",
				"image": "static/img/exhibit/4-2.jpg"
			},
			{
				"id": "4",
				"iid": "4-3",
				"category": "力學",
				"title": "善變的混沌擺",
				"title_en": "Capricious Chaotic Pendulum",
				"description": "轉動中央的旋鈕，讓T字形的下方支臂朝上垂直，等到所有的短桿都靜止了，放鬆旋鈕讓支臂開始落下並旋轉，多做幾次，設法將開始的狀況調到幾乎一模一樣，看後續的擺動會不會相同？也可以從不同的角度開始，觀察如果將開始的狀況調到幾乎相同，後續會不會相同？是否每次實驗在初始狀況中，都會有微小的差異存在？",
				"image": "static/img/exhibit/4-3.jpg"
			},
			{
				"id": "5",
				"iid": "5-1",
				"category": "流體力學",
				"title": "水中龍捲風",
				"title_en": "Tornado in Water",
				"description": "先按下右邊的按鈕，觀察下方螺貝開始高速旋轉後，上方的水面會如何變化？旋轉龍捲出現後，是從上到下發展，還是由下往上發展？按下左邊的按鈕，當短槳形式的螺旋槳開始高速帶動水流，觀察上方的水面變化，第二次按壓會有什麼結果？為什麼會產生大量氣泡？兩個筒子中，由上到下的龍捲中心是什麼物理狀況？",
				"image": "static/img/exhibit/5-1.jpg"
			},
			{
				"id": "5",
				"iid": "5-2",
				"category": "流體力學",
				"title": "綠建築的祕密",
				"title_en": "Secrets of Green Building",
				"description": "台灣每年有一半以上的時間會讓人覺得熱，因此使用空調消耗了大量的能源，那我們想想看，一棟建築要如何在空調上節能減碳？成功大學綠色魔法學校是台灣第一座「零碳綠建築」 ，現在就來看看綠建築的祕密吧！流體力學模擬冷熱空氣對流，熱空氣上升、冷空氣下降，設計出進風口低、出風口高。熱空氣以煙囪效應向上排除，並帶進建築物開口的冷空氣。浮力通風塔利用浮力通風的原理，打造節能又通風的環境。",
				"image": "static/img/exhibit/5-2.jpg"
			},
			{
				"id": "6",
				"iid": "6-1",
				"category": "波動",
				"title": "同步共振與機械手臂",
				"title_en": "Sympathetic Resonance and Robotic Arm",
				"description": "本展品中的機械手臂由台達提供，展品按公告時間自動啟動。機械手臂可以作精密移動，準確夾物，將每個節拍器輪流移至發條區上緊發條，然後橫掃而過，讓每個節拍器開始來回擺動震盪，一開始節拍器各擺各的，但是過不了多久所有的節拍器就會達到「同步擺動」的規律狀況，畫面和聲音都相當震撼！請觀察從隨機擺動開始，到完全同步，大約需要多少時間？為何節拍器彼此之間可以「互相溝通」，最後達到同步？這個展示的關鍵秘訣在何處？",
				"image": "static/img/exhibit/6-1.jpg"
			},
			{
				"id": "6",
				"iid": "6-2",
				"category": "波動",
				"title": "波的足跡",
				"title_en": "The Trace of Waves",
				"description": "本展品上方彈簧展示「縱波」的產生和傳遞，下方展示的是「橫波」的產生和傳遞。將上方的操縱桿前後猛的一推，會看到彈簧中出現疏密相間但向前移動的「縱波」。如果對面的尾端固定，這一邊創造的縱波撞擊對面固定端，會產生何種現象？反射的波在相位上是否會有變化？兩邊的參觀者同時用力一推，會看到各人創造的縱波彼此穿越，穿越前和穿越後這兩個縱波的強度和形狀有無變化？下方的操縱桿可以橫向猛的一晃，會看到彈簧中出現左右擺動但向前移動的「橫波」，重複上面一人和兩人的操作，觀察橫波的反射和穿越。",
				"image": "static/img/exhibit/6-2.jpg"
			},
			{
				"id": "6",
				"iid": "6-3",
				"category": "波動",
				"title": "垂直的蛇擺",
				"title_en": "Vertical Snake Pendulum ",
				"description": "單擺的週期和「擺長」及當地的「重力加速度」有關，這項展品中各個小球的擺長不同，但是垂直懸吊，轉動把手推動所有的小球開始擺盪，因為各個球的擺動週期不同，就會出現不同的振盪和波浪組合，操作者可以退後幾步，好好欣賞。想想看，每個小球的擺動週期都是固定的，由擺長決定，為何整體蛇擺的花樣會不斷改變？這個展示是用藝術的手法，來表達科學的內涵！",
				"image": "static/img/exhibit/6-3.jpg"
			},
			{
				"id": "6",
				"iid": "6-4",
				"category": "波動",
				"title": "水平的蛇擺",
				"title_en": "Horizontal Snake Pendulum",
				"description": "這個蛇擺是由好幾個水平懸吊的小球構成，每個小球的擺長不同，因此振盪週期也就不相同。逆時針轉動操縱桿，讓蛇擺開始擺盪，可以從左右兩邊觀察波形變化，想想看，每個小球的擺動週期都是固定的，由擺長決定，但為何整體蛇擺的花樣不斷改變？",
				"image": "static/img/exhibit/6-4.jpg"
			},
			{
				"id": "7",
				"iid": "7-1",
				"category": "近代物理",
				"title": "看不見的能量-紅外線",
				"title_en": "The Invisible Energy - Infrared Radiation",
				"description": "三個攝影機將現場觀眾影像投影在前方的三個銀幕上，左邊是「可見光」影像，右邊是「近紅外」影像，而中間是「中紅外」影像。哪個波段是人眼最熟悉的？哪個波段需要加上額外光源才看得到前方的人們？哪個波段是我們人類發出最強輻射的波段？在正前方「中紅外」的影像裡，為何會出現好些戴著墨鏡的黑道人物？移過一塊壓克力板，看看中紅外線能否通過？將雙手手心分別壓在冷熱金屬上幾秒鐘，舉起來看影像中的雙手，會出現何種畫面？點亮展示板上的三盞燈，可見光裡很清楚，但看看「中紅外」影像中，哪個燈泡不見了？為什麼？",
				"image": "static/img/exhibit/7-1.jpg"
			},
			{
				"id": "7",
				"iid": "7-2",
				"category": "近代物理",
				"title": "繽紛的光譜世界",
				"title_en": "A Colorful World of Optical  Spectra",
				"description": "移動筒狀的光譜儀，依次對準前方的光源，從觀景窗中觀察各個光源的分光光譜，不同光譜有什麼差別？有些光源會顯現明亮的彩色線條，有些光源沒有線條，就是一片繽紛的連續彩色，有些光源幾乎只有單一顏色，這些差別是怎麼來的？觀察這些光源的形狀和規格，針對這些光源的發光原理，能夠推斷出什麼結論？",
				"image": "static/img/exhibit/7-2.jpg"
			},
			{
				"id": "7",
				"iid": "7-3",
				"category": "近代物理",
				"title": "電光效應 - 電力小英雄",
				"title_en": "Electro-optic Effect - Little Heroes of Electricity",
				"description": "請站到三面螢幕前方的標記上，揮動雙手啟動遊戲，就可以在能源旅途上回答問題了！這個遊戲由台達的工程師創意發展，可以一個人體驗，也可以兩人同時參加，一人模式可以直接選擇「太陽能發電」或是「火力發電」開始遊戲，兩人模式中一人進行主遊戲，第二人負責操作手搖發電機，讓螢幕左方燈泡發亮且提升左邊畫面中電池的充電狀態。",
				"image": "static/img/exhibit/7-3.jpg"
			},
			{
				"id": "8",
				"iid": "8-1",
				"category": "光學",
				"title": "魔幻像差",
				"title_en": "Magical Optical Aberration",
				"description": "面前的凹面鏡會將我們自己的「倒立實像」展現出來，但是這個照鏡子的過程是否可以讓我們看到們自己的「正立虛像」？好像看不到！但是將手機的攝影功能打開，對著凹面鏡從中央逐漸接近，到一定距離處，就會看到一個自己的正立虛像出現在手機裡！這時逐漸將手機拉遠，會看到正立虛像在經過一個「界線」之後，瞬間轉成倒立實像，你能夠從這個變化的地方推斷出來這個凹面鏡的焦距嗎？ ",
				"image": "static/img/exhibit/8-1.jpg"
			},
			{
				"id": "8",
				"iid": "8-2",
				"category": "光學",
				"title": "七彩光學遊戲台",
				"title_en": "A Colorful Table of Geometric Optics",
				"description": "按下按鈕，就會看到多條彩色雷射光線射出，前方盡頭開始煙霧飄渺，明顯看出雷射光的路徑，此時可以移動桌面上的三稜鏡、四方鏡，以及各種透鏡和面鏡，觀察反射、折射、繞射等幾何光學的效應。想想看，為何要有煙霧？這些飄渺的煙霧是來自什麼物質？如何產生這些煙霧的？",
				"image": "static/img/exhibit/8-2.jpg"
			},
			{
				"id": "8",
				"iid": "8-3",
				"category": "光學",
				"title": "水往高處流？",
				"title_en": "Water Running Upward？",
				"description": "水往低處流是大家熟悉的現象，但是在這個展品中，我們卻看到昏暗閃爍的燈光下，水滴好像不是往下掉，反而往上走？這是為什麼？是不是和閃爍的燈光有關？我們看電視影片，有時看到馬車往前走，但輪子會往後轉，和這個展品是同樣的道理，也就是你能看到它的時間有限，同時看到的影像間斷地很有規律，就會產生「倒過來走」的錯覺了。",
				"image": "static/img/exhibit/8-3.jpg"
			},
			{
				"id": "8",
				"iid": "8-4",
				"category": "光學",
				"title": "實像和虛像",
				"title_en": "Real Image and Virtual Image",
				"description": "觀察面前的凹面鏡，看看自己在鏡中呈現的像是正立的還是倒立的？這是「實像」還是「虛像」？拿一隻手電筒，或者打開手機上的照明燈，照在自己額頭上，像中的額頭上是否會出現一個同樣的光點？再將照明燈照在像中的額頭上，看看自己的額頭上是否會出現同樣的光點？這個比較可以讓我們深切體認「實像」的意義！再將手機的攝影功能打開，對著凹面鏡從中央逐漸接近，會看到一個自己的正立像出現在手機裡，這是「實像」還是「虛像」？呈像的位置在哪裡？逐漸將手機拉遠，會看到正立的像在經過一個「界線」之後，瞬間轉成倒立的像，請問這個「界線」是什麼？",
				"image": "static/img/exhibit/8-4.jpg"
			}
		],
		"game": [
			{
				"id": 1,
				"title": "尋找出口 - 磁力",
				"description": "磁鐵是可以吸引鐵並於其外產生磁場的物體，指向北方的磁極稱為指北極或N極，指向南方的磁極為指南極或S極。磁鐵異極則相吸，同極則排斥。磁鐵分作永久磁鐵與非永久磁鐵，非永久性磁鐵通常是以電磁鐵的形式產生，利用電流來強化磁場。",
				"image": "static/img/game/1.png"
			},
			{
				"id": 2,
				"title": "飛天羅巴 - 萬有引力",
				"description": "重力又稱萬有引力，是指具有質量的物體之間相互吸引的作用，也是物體重量的來源。星球間的質量不同，所產生的引力也不同，月球的質量只有地球的六分之一，所以重力也是地球的六分之一。在月球上用力跳躍，可以比地球跳的更高更遠。",
				"image": "static/img/game/2.png"
			},
			{
				"id": 3,
				"title": "一杯剛好 - 摩擦力",
				"description": "一物體在另一物體表面上滑動或將要滑動時，在接觸面上會產生阻止相對運動的作用力，稱為摩擦力，摩擦力會將物體的動能，轉換成為熱能。摩擦力是來自於帶電粒子之間的電磁力，包括電子、質子、原子和分子之間的作用力。",
				"image": "static/img/game/3.png"
			},
			{
				"id": 4,
				"title": "水管接龍 - 流體力學",
				"description": "流體力學是力學的一門分支，是研究流體（包含氣體、液體及電漿體）現象以及相關力學行為的科學。流體運動的方式受以下因素的影響：流體的性質 、流道的形狀 、 外加壓力的大小，而流動和形變是流體最顯著的性質。",
				"image": "static/img/game/4.png"
			},
			{
				"id": 5,
				"title": "拆房大隊 - 能量守恆",
				"description": "物體(或系統)運動狀態保持不變的情況下，由於能量是守恆的，不能被創造或消滅，只能夠轉換為不同的形式。例如：當火箭起飛之時，有大量氣體向下垂直噴出。根據動量守恆原理，火箭所獲得向上的動量，大小應剛好與氣體向下的動量相等。",
				"image": "static/img/game/5.png"
			},
			{
				"id": 6,
				"title": "蛇形高手 - 簡諧運動",
				"description": "蛇擺由數個單擺組成，擺動後出現行進波的形狀。波長隨著時間變化進入紊亂的狀態，看不出波形。接著單數球與雙數球分開兩邊的過程，彷彿重複前半週期的狀態，但行進波的方向變了，與前半週期的行進方向相反，最後回到初始的一直線狀態。",
				"image": "static/img/game/6.png"
			},
			{
				"id": 7,
				"title": "光的旅程 - 光線",
				"description": "當光由一介質進入另一介質，回到原來介質的光，稱之反射，進入另一介質，稱之為折射 。當光線行進遇到不同介質時，例如：平滑的鏡面，會讓大多數的光線反射回來，平行的光束射向平滑的鏡面後依然平行的射出。",
				"image": "static/img/game/7.png"
			},
			{
				"id": 8,
				"title": "榮耀時刻",
				"description": "當你通過七關關卡，獲得七面勳章，只要再答對最後三個問題，就可獲得最終的至尊科學獎章。",
				"image": "static/img/game/8.png"
			}
		],
		"voiceguide": [
			{
				"id": "1-1",
				"category": "序展",
				"title": "物理之門",
				"title_en": "Physics Gate"
			},
			{
				"id": "1-2",
				"category": "序展",
				"title": "總部：物理小老師",
				"title_en": "Headquarter:Physics Tutor"
			},
			{
				"id": "1-3",
				"category": "序展",
				"title": "互動學習桌（後端學習系統）",
				"title_en": "Interactive Learning Desk (Back End LearningSystem)"
			},
			{
				"id": "1-4",
				"category": "序展",
				"title": "集合廣場",
				"title_en": "Physics Plaza"
			},
			{
				"id": "1-5",
				"category": "序展",
				"title": "精神堡壘",
				"title_en": "Fortress of Spirit"
			},
			{
				"id": "2-1",
				"category": "電磁學",
				"title": "人造磁雲",
				"title_en": "Artificial Magnetic Cloud",
				"description": "握著磁鐵，從容器外面底部緩慢將鐵粉吸引上來，然後將磁鐵拉開，鐵粉就會飄舞而下，形成一片夢幻般的「磁雲」！再用磁鐵吸引尚未完全沈澱的鐵粉，鐵粉又會重新聚集，此時從對面看，會發現鐵粉依據特殊的線條向外排列延伸，這些線條是什麼？"
			},
			{
				"id": "2-2",
				"category": "電磁學",
				"title": "物質的磁性",
				"title_en": "Magnetic Property of Matter",
				"description": "輪流拿起不同的棒子，保持水平，從上往下通過磁場拱門，這個外加磁場會對哪一根棒子產生拉扯？產生拉扯時，是吸引力還是排斥力？為什麼？有拉扯感覺的棒子，是它自己本來就有磁力嗎？嘗試完了每一根棒子之後，比較不同材質的棒子受到拉扯的有無和強弱，以瞭解物質的磁性。在看過旁邊螢幕中的「科學介紹」後，想想看，磁場拱門裡裝的是什麼東西？具有何種磁性？"
			},
			{
				"id": "2-3",
				"category": "電磁學",
				"title": "若即若離的磁力",
				"title_en": "Magnetic Force - Attractive or Repulsive ",
				"description": "圓環中央的磁鐵和外面的磁鐵互相吸引，可是圓環周邊的6個小磁鐵卻和外面的磁鐵互相排斥，在這種組合下，將圓環靠近外面的磁鐵，會發生什麼事？會互相吸引還是互相排斥？靠近之後再拉遠，會發生什麼事？如果不是在同一平面上，而是在垂直方向上互相接近，又會出現何種現象？吸進來還是彈出去？如果將這個垂直裝置反過來呢？外加磁鐵會不會掉下去？"
			},
			{
				"id": "2-4",
				"category": "電磁學",
				"title": "違反能量守恆的碰撞？",
				"title_en": "Collision that Violates Energy Conservation？",
				"description": "按下按鈕，看小球從平台上緩緩滾下，撞向一串五個小球，這一串小球的最後一個球獲得撞擊的能量，竟然能跑到比原先小球更高的地方！這表示它獲得了更多的位能，但這不是違反了能量守恆的原理嗎？發揮你福爾摩斯的觀察能力，看看這個顯然違反能量守恆的過程，找出關鍵秘訣在哪裡？"
			},
			{
				"id": "2-5",
				"category": "電磁學",
				"title": "磁力抗拒重力？",
				"title_en": "Magnetic Force against Gravitational Force",
				"description": "拿起桌上的螺圈，吸引展品箱中的小球，將小球輪流帶到各個管子的上方，讓它掉入管中，觀察小球在不同管子中掉出所需的時間是否相同？如果不同，哪個管子所花的時間最短？哪根管子所花的時間最久？為什麼？如果你身材夠高，可以在放入小球後，從管子上方往裡面看，觀察小球緩慢但均勻下落的過程。"
			},
			{
				"id": "2-6",
				"category": "電磁學",
				"title": "看不見的吸引力",
				"title_en": "The Invisible Attractive Force",
				"description": "先按下按鈕，讓線圈通電，再輪流拿不同材質的棒子插入線圈中央，感受一下哪種材質的棒子會受到拉扯？是吸引力還是排斥力？為什麼？如果線圈沒有通電，會產生同樣的現象嗎？比較各個棒子的材質，就會知道不同物質的磁性表現。"
			},
			{
				"id": "3-1",
				"category": "運動學",
				"title": "永不停歇的風火輪",
				"title_en": "Long-Lasting Wheels of Wind and Fire",
				"description": "桌面中央是一個高速轉動的圓盤，將圓球從這一端滾向那一端，圓球會走出什麼軌跡？將圓球擺在圓盤的中心，當它逐漸離開中心，它的「公轉」和「自轉」會產生什麼變化？為什麼？試著將各個圓盤或圓環放上轉盤，看能不能找出一個讓這些物體持久轉動的方法？試著讓圓盤或圓環接觸轉盤時反向滾動，是否有幫助？"
			},
			{
				"id": "3-2",
				"category": "運動學",
				"title": "轉動慣量與轉動動能",
				"title_en": "Moment of Inertia and Rotational Energy",
				"description": "圓盤兩側各有把手，可以反向轉動調整重錘的位置，以改變整個圓盤的質量分布。把一個圓盤的重錘都集中到中心，另一個圓盤的重錘分散到最外緣，將兩個圓盤從滑道頂端同時放開，誰會先到終點？為什麼？"
			},
			{
				"id": "3-3",
				"category": "運動學",
				"title": "圓球‧圓盤‧圓環，誰跑得最快？",
				"title_en": "A Sphere, a Disc, and A Ring, which one runs the fastest?",
				"description": "圓球、圓盤，和圓環三者質量相同，從同一高度放開，讓它們滾動而下，誰會先到終點？為什麼？"
			},
			{
				"id": "3-4",
				"category": "運動學",
				"title": "滾球接龍",
				"title_en": "Rolling Ball Relay",
				"description": "將各種磁性軌道用不同方式排列在牆面上，形成一串向下滑動的路徑，看誰做的軌道能讓小球滾最久？這個展品顯示的是哪兩種能量之間的轉換？"
			},
			{
				"id": "4-1",
				"category": "力學",
				"title": "科氏力大型咖啡杯",
				"title_en": "Grandiose Coffee Cup of Coriolis Force",
				"description": "按照解說人員的指示，依序進入咖啡杯坐定，在咖啡杯開始轉動之前，先觀察地面小球直線滾動的軌跡，在咖啡杯達到穩定轉動之後，再觀察小球在地面滾動的路徑，和先前有無差異？若有，這個改變是真的還是假的？在轉動之前，解說人員讓中央垂降的「佛科擺」開始擺動，開始轉動之後，觀察擺動平面有無變化？若有，這個變化是真的還是假的？這些變化都是「科氏力」造成的，而這個「科氏力」是一個真實的力嗎？離開咖啡杯之後，到欄杆外面觀賞銀幕上的四分割畫面，看別人在體驗時，滾球路徑或單擺平面從「轉動座標系」和「靜止座標系」觀察的差異。"
			},
			{
				"id": "4-2",
				"category": "力學",
				"title": "忠實的牛頓擺",
				"title_en": "Faithful Newton's Cradle",
				"description": "牛頓擺看來簡單，就是五個小球彼此互撞，但它其實有很多種展示方法，可以測驗你對「能量」和「動量」的瞭解！一個小球拉起來撞回去，是撞飛一個球，還是撞飛四個球？為什麼？兩個球拉起來撞下去，會怎樣？到三個球和四個球，就真的需要思考了！另外從兩邊各拉起一個球，撞下去會不會彈回去？如果會彈起來，那是因為小球「撞牆」的緣故，還是因為左右小球彼此能量互換？要怎麼測試才會知道？右邊的五個球質量一樣，但是都較輕，左邊的五個球質量不同，在這些系統中，重複以上過程，會有什麼結果？"
			},
			{
				"id": "4-3",
				"category": "力學",
				"title": "善變的混沌擺",
				"title_en": "Capricious Chaotic Pendulum",
				"description": "轉動中央的旋鈕，讓T字形的下方支臂朝上垂直，等到所有的短桿都靜止了，放鬆旋鈕讓支臂開始落下並旋轉，多做幾次，設法將開始的狀況調到幾乎一模一樣，看後續的擺動會不會相同？也可以從不同的角度開始，觀察如果將開始的狀況調到幾乎相同，後續會不會相同？是否每次實驗在初始狀況中，都會有微小的差異存在？"
			},
			{
				"id": "5-1",
				"category": "流體力學",
				"title": "水中龍捲風",
				"title_en": "Tornado in Water",
				"description": "先按下右邊的按鈕，觀察下方螺貝開始高速旋轉後，上方的水面會如何變化？旋轉龍捲出現後，是從上到下發展，還是由下往上發展？按下左邊的按鈕，當短槳形式的螺旋槳開始高速帶動水流，觀察上方的水面變化，第二次按壓會有什麼結果？為什麼會產生大量氣泡？兩個筒子中，由上到下的龍捲中心是什麼物理狀況？"
			},
			{
				"id": "5-2",
				"category": "流體力學",
				"title": "綠建築的祕密",
				"title_en": "Secrets of Green Building",
				"description": "台灣每年有一半以上的時間會讓人覺得熱，因此使用空調消耗了大量的能源，那我們想想看，一棟建築要如何在空調上節能減碳？成功大學綠色魔法學校是台灣第一座「零碳綠建築」 ，現在就來看看綠建築的祕密吧！流體力學模擬冷熱空氣對流，熱空氣上升、冷空氣下降，設計出進風口低、出風口高。熱空氣以煙囪效應向上排除，並帶進建築物開口的冷空氣。浮力通風塔利用浮力通風的原理，打造節能又通風的環境。"
			},
			{
				"id": "6-1",
				"category": "波動",
				"title": "同步共振與機械手臂",
				"title_en": "Sympathetic Resonance and Robotic Arm",
				"description": "本展品中的機械手臂由台達提供，展品按公告時間自動啟動。機械手臂可以作精密移動，準確夾物，將每個節拍器輪流移至發條區上緊發條，然後橫掃而過，讓每個節拍器開始來回擺動震盪，一開始節拍器各擺各的，但是過不了多久所有的節拍器就會達到「同步擺動」的規律狀況，畫面和聲音都相當震撼！請觀察從隨機擺動開始，到完全同步，大約需要多少時間？為何節拍器彼此之間可以「互相溝通」，最後達到同步？這個展示的關鍵秘訣在何處？"
			},
			{
				"id": "6-2",
				"category": "波動",
				"title": "波的足跡",
				"title_en": "The Trace of Waves",
				"description": "本展品上方彈簧展示「縱波」的產生和傳遞，下方展示的是「橫波」的產生和傳遞。將上方的操縱桿前後猛的一推，會看到彈簧中出現疏密相間但向前移動的「縱波」。如果對面的尾端固定，這一邊創造的縱波撞擊對面固定端，會產生何種現象？反射的波在相位上是否會有變化？兩邊的參觀者同時用力一推，會看到各人創造的縱波彼此穿越，穿越前和穿越後這兩個縱波的強度和形狀有無變化？下方的操縱桿可以橫向猛的一晃，會看到彈簧中出現左右擺動但向前移動的「橫波」，重複上面一人和兩人的操作，觀察橫波的反射和穿越。"
			},
			{
				"id": "6-3",
				"category": "波動",
				"title": "垂直的蛇擺",
				"title_en": "Vertical Snake Pendulum ",
				"description": "單擺的週期和「擺長」及當地的「重力加速度」有關，這項展品中各個小球的擺長不同，但是垂直懸吊，轉動把手推動所有的小球開始擺盪，因為各個球的擺動週期不同，就會出現不同的振盪和波浪組合，操作者可以退後幾步，好好欣賞。想想看，每個小球的擺動週期都是固定的，由擺長決定，為何整體蛇擺的花樣會不斷改變？這個展示是用藝術的手法，來表達科學的內涵！"
			},
			{
				"id": "6-4",
				"category": "波動",
				"title": "水平的蛇擺",
				"title_en": "Horizontal Snake Pendulum",
				"description": "這個蛇擺是由好幾個水平懸吊的小球構成，每個小球的擺長不同，因此振盪週期也就不相同。逆時針轉動操縱桿，讓蛇擺開始擺盪，可以從左右兩邊觀察波形變化，想想看，每個小球的擺動週期都是固定的，由擺長決定，但為何整體蛇擺的花樣不斷改變？"
			},
			{
				"id": "7-1",
				"category": "近代物理",
				"title": "看不見的能量-紅外線",
				"title_en": "The Invisible Energy - Infrared Radiation",
				"description": "三個攝影機將現場觀眾影像投影在前方的三個銀幕上，左邊是「可見光」影像，右邊是「近紅外」影像，而中間是「中紅外」影像。哪個波段是人眼最熟悉的？哪個波段需要加上額外光源才看得到前方的人們？哪個波段是我們人類發出最強輻射的波段？在正前方「中紅外」的影像裡，為何會出現好些戴著墨鏡的黑道人物？移過一塊壓克力板，看看中紅外線能否通過？將雙手手心分別壓在冷熱金屬上幾秒鐘，舉起來看影像中的雙手，會出現何種畫面？點亮展示板上的三盞燈，可見光裡很清楚，但看看「中紅外」影像中，哪個燈泡不見了？為什麼？"
			},
			{
				"id": "7-2",
				"category": "近代物理",
				"title": "繽紛的光譜世界",
				"title_en": "A Colorful World of Optical  Spectra",
				"description": "移動筒狀的光譜儀，依次對準前方的光源，從觀景窗中觀察各個光源的分光光譜，不同光譜有什麼差別？有些光源會顯現明亮的彩色線條，有些光源沒有線條，就是一片繽紛的連續彩色，有些光源幾乎只有單一顏色，這些差別是怎麼來的？觀察這些光源的形狀和規格，針對這些光源的發光原理，能夠推斷出什麼結論？"
			},
			{
				"id": "7-3",
				"category": "近代物理",
				"title": "電光效應 - 電力小英雄",
				"title_en": "Electro-optic Effect - Little Heroes of Electricity",
				"description": "請站到三面螢幕前方的標記上，揮動雙手啟動遊戲，就可以在能源旅途上回答問題了！這個遊戲由台達的工程師創意發展，可以一個人體驗，也可以兩人同時參加，一人模式可以直接選擇「太陽能發電」或是「火力發電」開始遊戲，兩人模式中一人進行主遊戲，第二人負責操作手搖發電機，讓螢幕左方燈泡發亮且提升左邊畫面中電池的充電狀態。"
			},
			{
				"id": "8-1",
				"category": "光學",
				"title": "魔幻像差",
				"title_en": "Magical Optical Aberration",
				"description": "面前的凹面鏡會將我們自己的「倒立實像」展現出來，但是這個照鏡子的過程是否可以讓我們看到們自己的「正立虛像」？好像看不到！但是將手機的攝影功能打開，對著凹面鏡從中央逐漸接近，到一定距離處，就會看到一個自己的正立虛像出現在手機裡！這時逐漸將手機拉遠，會看到正立虛像在經過一個「界線」之後，瞬間轉成倒立實像，你能夠從這個變化的地方推斷出來這個凹面鏡的焦距嗎？ "
			},
			{
				"id": "8-2",
				"category": "光學",
				"title": "七彩光學遊戲台",
				"title_en": "A Colorful Table of Geometric Optics",
				"description": "按下按鈕，就會看到多條彩色雷射光線射出，前方盡頭開始煙霧飄渺，明顯看出雷射光的路徑，此時可以移動桌面上的三稜鏡、四方鏡，以及各種透鏡和面鏡，觀察反射、折射、繞射等幾何光學的效應。想想看，為何要有煙霧？這些飄渺的煙霧是來自什麼物質？如何產生這些煙霧的？"
			},
			{
				"id": "8-3",
				"category": "光學",
				"title": "水往高處流？",
				"title_en": "Water Running Upward？",
				"description": "水往低處流是大家熟悉的現象，但是在這個展品中，我們卻看到昏暗閃爍的燈光下，水滴好像不是往下掉，反而往上走？這是為什麼？是不是和閃爍的燈光有關？我們看電視影片，有時看到馬車往前走，但輪子會往後轉，和這個展品是同樣的道理，也就是你能看到它的時間有限，同時看到的影像間斷地很有規律，就會產生「倒過來走」的錯覺了。"
			},
			{
				"id": "8-4",
				"category": "光學",
				"title": "實像和虛像",
				"title_en": "Real Image and Virtual Image",
				"description": "觀察面前的凹面鏡，看看自己在鏡中呈現的像是正立的還是倒立的？這是「實像」還是「虛像」？拿一隻手電筒，或者打開手機上的照明燈，照在自己額頭上，像中的額頭上是否會出現一個同樣的光點？再將照明燈照在像中的額頭上，看看自己的額頭上是否會出現同樣的光點？這個比較可以讓我們深切體認「實像」的意義！再將手機的攝影功能打開，對著凹面鏡從中央逐漸接近，會看到一個自己的正立像出現在手機裡，這是「實像」還是「虛像」？呈像的位置在哪裡？逐漸將手機拉遠，會看到正立的像在經過一個「界線」之後，瞬間轉成倒立的像，請問這個「界線」是什麼？"
			}
		],
		"learning": [
			{
				"filename": "物理廳低年級學習單",
				"size": "167KB",
				"url": "static/learn-sheet/learn-sheet-easy.pdf",
				"date": "2017/2/22"
			},
			{
				"filename": "物理廳中年級學習單",
				"size": "140KB",
				"url": "static/learn-sheet/learn-sheet-medium.pdf",
				"date": "2017/2/22"
			},
			{
				"filename": "物理廳高年級學習單",
				"size": "158KB",
				"url": "static/learn-sheet/learn-sheet-difficult.pdf",
				"date": "2017/2/22"
			}
		],
		"youtube": [
			{
				"title": "1. 前言導覽",
				"url": "https://www.youtube.com/watch?v=jnuv4PPyW4Y"
			},
			{
				"title": "2. 人造磁雲",
				"url": "https://www.youtube.com/watch?v=NEkDt5bdbXI"
			},
			{
				"title": "3. 物質的磁性 (強力萬磁王)",
				"url": "https://www.youtube.com/watch?v=moUREtsaNSU"
			},
			{
				"title": "4. 若即若離的磁力 (隔空感應)",
				"url": "https://www.youtube.com/watch?v=qkaIifOjA_8"
			},
			{
				"title": "5. 違反能量守恆的碰撞？ (磁力彈弓)",
				"url": "https://www.youtube.com/watch?v=DzYDg0Qndl4"
			},
			{
				"title": "6. 看不見的吸引力 (感應電棒)",
				"url": "https://www.youtube.com/watch?v=IDpi3uyCDtM"
			},
			{
				"title": "7. 磁力抗拒重力？ (磁力管) ",
				"url": "https://www.youtube.com/watch?v=MKT6kO1JKGo"
			},
			{
				"title": "8. 圓球•圓盤•圓環，誰跑得最快？ (轉動慣量)",
				"url": "https://www.youtube.com/watch?v=_C4uUAta8ZU"
			},
			{
				"title": "9. 永不停歇的風火輪 (滾動競賽）",
				"url": "https://www.youtube.com/watch?v=XYCX0NGqm8c"
			},
			{
				"title": "10. 滾球接龍",
				"url": "https://www.youtube.com/watch?v=IfsW6siwViw"
			},
			{
				"title": "11. 水中龍捲風",
				"url": "https://www.youtube.com/watch?v=VSZPpH1U3bE"
			},
			{
				"title": "12. 忠實的牛頓擺",
				"url": "https://www.youtube.com/watch?v=OFbZW8D4Xcg"
			},
			{
				"title": "13. 空中發射",
				"url": "https://www.youtube.com/watch?v=G3diHFMBo-8"
			},
			{
				"title": "14. 科氏力大型咖啡杯",
				"url": "https://www.youtube.com/watch?v=NzgfwRn0S30"
			},
			{
				"title": "15. 善變的混沌擺",
				"url": "https://www.youtube.com/watch?v=MpPqycPh190"
			},
			{
				"title": "16. 善變的蛇擺",
				"url": "https://www.youtube.com/watch?v=8u1dK-g2Voc"
			},
			{
				"title": "17. 波的足跡",
				"url": "https://www.youtube.com/watch?v=Vwmuh1KxbIc"
			},
			{
				"title": "18. 同步共振與機械手臂",
				"url": "https://www.youtube.com/watch?v=Y7tWuZse4U8"
			},
			{
				"title": "19. 實像與虛像 (神奇焦點)",
				"url": "https://www.youtube.com/watch?v=uE1RRZqaEjo"
			},
			{
				"title": "20. 魔幻像差",
				"url": "https://www.youtube.com/watch?v=EcH0geRMRSs"
			},
			{
				"title": "21. 看不見的能量-紅外線 (光學實驗台-熱顯像儀)",
				"url": "https://www.youtube.com/watch?v=QP_9TEZUQe0"
			},
			{
				"title": "22. 水往高處流？ (水往上流)",
				"url": "https://www.youtube.com/watch?v=qGMxrv60Ahk"
			},
			{
				"title": "23. 繽紛的光譜世界",
				"url": "https://www.youtube.com/watch?v=DPnyUDcAp3M"
			},
			{
				"title": "24. 電光效應-電力小英雄",
				"url": "https://www.youtube.com/watch?v=wOHdVLEQsvc"
			},
			{
				"title": "25. 七彩光學遊戲台",
				"url": "https://www.youtube.com/watch?v=srkmg0ipLI4"
			}
		],
		"source": {
			"TRAFFIC_MAP_URL": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3640.427123971107!2d120.66336231541106!3d24.156748879047417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34693d78069134cb%3A0xf7a65bba8f974f36!2z5ZyL56uL6Ieq54S256eR5a245Y2a54mp6aSo!5e0!3m2!1szh-TW!2stw!4v1489550639390",
			"TRAFFIC_SRC_MAP_URL": "https://www.google.com.tw/maps/place/%E5%9C%8B%E7%AB%8B%E8%87%AA%E7%84%B6%E7%A7%91%E5%AD%B8%E5%8D%9A%E7%89%A9%E9%A4%A8/@24.1559203,120.6651057,16.92z/data=!4m8!1m2!2m1!1z5ZyL56uL6Ieq54S256eR5a245Y2a54mp6aSo!3m4!1s0x34693d78069134cb:0xf7a65bba8f974f36!8m2!3d24.1572335!4d120.6660606",
			"MAP_IMG_URL": "static/img/map.png",
			"FOOTER_PRIVACY_URL": "http://www.nmns.edu.tw/ch/index/privacy.htm",
			"ABOUT_RESERVATION_URL": "http://cal.nmns.edu.tw/NMNS_Cal/Detail_Ann.aspx?ANID=12103",
			"FOOTER_REF_HOMEPAGE_URL": "http://www.nmns.edu.tw/",
			"FOOTER_REF_IG_URL": "https://www.instagram.com/nmnstw/",
			"FOOTER_REF_U2_URL": "https://www.youtube.com/user/NMNSTW1",
			"FOOTER_REF_FB_URL": "https://www.facebook.com/COBOFANS",
			"APP_ANDROID_URL": "https://play.google.com/store/apps/details?id=com.nmns.physical_world",
			"APP_IOS_URL": "https://itunes.apple.com/tw/app/%E7%89%A9%E7%90%86%E4%B8%96%E7%95%8C/id1219863796"
		},
		"strings": {
			"TOGGLE_LOCALE": "ENGLISH",
			"HOME_TITLE1": "啟發好奇，探索奧秘",
			"HOME_TITLE2": "成為最好的科學教育養分",
			"HOME_ACTION_TITLE": "了解物理廳訊息",
			"VOICE_TITLE": "物理世界展場語音導覽",
			"VOICE_GUIDE": "語音導覽",
			"VOICE_KEY_CLEAR_BTN": "清除",
			"VOICE_KEY_CONFIRM_BTN": "確認",
			"VOICE_KEY_BACK_BTN": "回上頁",
			"VOICE_KEY_TIP_TXT": "請輸入語音導覽編號後按下確認鍵",
			"VOICE_KEY_ERRORMSG_TXT": "查無此編號",
			"VOICE_PLAY_CLOSE_BTN": "關閉",
			"VOICE_PLAY_BACK_BTN": "重新選擇",
			"VOICE_PLAY_HOME_BTN": "回到首頁",
			"TRAFFIC_SRC_MAP_TXT": "檢視較大的地圖",
			"TRAFFIC_DESCRIPTION": "科博館緊鄰臺中市重要道路臺灣大道，交通便捷。自行開車於國道 1或3號高速公路，往臺中市方向順臺灣大道至科博館。使用大眾運輸，可搭乘高鐵、臺鐵、公路，轉乘公車到SOGO百貨或科博館站下車，請多利用大眾運輸來館。",
			"ABOUT_POS_TITLE": "展覽地點",
			"ABOUT_POS_TXT": "國立自然科學博物館\n科學中心4樓\n台中市北區館前路1號",
			"ABOUT_TIME_TITLE": "開放時間",
			"ABOUT_TIME_TXT": "週二至週日9:00-17:00\n(除過年期間外，寒暑假週一不休館)",
			"ABOUT_TICKET_TITLE": "門票資訊",
			"ABOUT_TICKET_TXT": "詳請參閱科博館官網票價表",
			"ABOUT_NOTICE_TITLE": "注意事項",
			"ABOUT_NOTICE_TXT": "因參觀品質和體驗機會考量，本展區採人數管制。",
			"ABOUT_NOTICE_RESERVATION_TXT": "預約參觀方式",
			"FOOTER_DESCRIPTION": "40453 臺中市北區館前路1號\n聯絡電話：(04) 2322-6940\n開放時間：週二至週日9:00-17:00\n(除過年期間外，寒暑假週一不休館)\n更新日期：2018-7-17",
			"FOOTER_PRIVACY": "隱私權及安全政策",
			"FOOTER_REF_TITLE": "相關連結",
			"FOOTER_REF_LINK1": "國立自然科學博物館",
			"FOOTER_REF_LINK2": "科博館 Instagram",
			"FOOTER_REF_LINK3": "科博館 YouTube",
			"FOOTER_REF_LINK4": "科博館 FB 粉絲頁",
			"FOOTER_FOLLOW_TITLE": "關注我們",
			"FOOTER_COPYRIGHT_ENG": "© Copyright National Museum of Natural Science. All Rights Reserved",
			"FOOTER_COPYRIGHT_ZH": "國立自然科學博物館版權所有",
			"LEARNING_TABLE_TITLE": "學習單下載",
			"LEARNING_FILENAME": "檔案名稱",
			"LEARNING_SIZE": "檔案大小",
			"LEARNING_DIFFICULTY": "難度",
			"LEARNING_DOWNLOAD": "文件下載",
			"LEARNING_DATE": "更新日期",
			"YOUTUBE_TABLE_TITLE": "Youtube 影片",
			"YOUTUBE_TITLE": "影片名稱",
			"APPLOGIN_TIP": "請輸入下列資料，即可使用產生的二維條碼至展場進行遊戲。\n(系統僅儲存加密後的個資，資料僅供遊戲過程使用)",
			"APPLOGIN_TIP2": "請使用二維條碼至展場遊戲機台進行遊戲。",
			"APPLOGIN_NAME": "姓名",
			"APPLOGIN_EMAIL": "Email",
			"APPLOGIN_GENDER": "性別",
			"APPLOGIN_MALE": "男",
			"APPLOGIN_FEMALE": "女",
			"APPLOGIN_AGE": "年齡",
			"APPLOGIN_UNDER12": "12歲以下",
			"APPLOGIN_OLDER60": "60歲以上",
			"APPLOGIN_REGISTER": "註冊",
			"APPLOGIN_RE_REGISTER": "重新註冊",
			"APPLOGIN_ERRORMSG_EMAIL": "請輸入有效的電子信箱",
			"APPLOGIN_ERRORMSG_NAME": "請輸入姓名",
			"APPLOGIN_ERRORMSG_GENDER": "請輸入性別",
			"APPLOGIN_ERRORMSG_AGE": "請輸入年齡",
			"HOME_APP_BTN_TIP": "物理世界導覽程式",
			"QUIZ_NEXT_QUESTION": "挑戰下一題",
			"QUIZ_CORRECT_MSG": "(O) 答對囉！",
			"QUIZ_INCORRECT_MSG": "(X) 答錯了...",
			"MODAL_CLOSE": "關閉",
			"ABOUT_APP_USER_TERMS_TITLE": "隱私權政策",
			"ABOUT_APP_USER_TERMS": "歡迎使用「國立自然科學博物館 - 物理世界」（以下簡稱本應用程式），為了讓您能夠安心使用本應用程式的各項服務與資訊，特此向您說明本系統的隱私權保護政策，以保障您的權益，請您詳閱下列內容：\n\n一、隱私權保護政策的適用範圍\n隱私權保護政策內容，包括本應用程式如何處理在您使用時收集到的個人資料。隱私權保護政策不適用於本應用程式以外的相關連結系統。\n二、個人資料的蒐集、處理及利用方式\n  •當您使用本應用程式之所提供之功能服務時，我們將視該服務功能性質，請您提供必要的個人資料，並在該特定目的範圍內處理及利用您的個人資料；非經您書面同意，本系統不會將個人資料用於其他用途。\n  •本應用程式在您使用現場遊戲註冊功能時，會在應用程式中加密儲存您所提供的姓名、電子郵件地址、性別以及年齡。此資料僅供現場遊戲記錄使用。\n  •於一般使用時，應用程式會記錄點選頁面，做為我們增進系統服務的參考依據，此記錄為內部應用，決不對外公佈。\n三、與第三人共用個人資料之政策\n本應用程式絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。\n前項但書之情形包括不限於：\n\n  •經由您書面同意。\n  •法律明文規定。\n  •為免除您生命、身體、自由或財產上之危險。\n  •與公務機關或學術研究機構合作，基於公共利益為統計或學術研究而有必要，且資料經過提當您在系統的行為，違反服務條款或可能損害或妨礙系統與其他使用者權益或導致任何人遭受損害時，經系統管理單位研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。\n  •有利於您的權益。\n  •本系統委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。\n\n四、隱私權保護政策之修正\n本系統隱私權保護政策將因應需求隨時進行修正，修正後的條款將刊登於系統上。",
			"ABOUT_APP_EMAIL_TITLE": "問題回報",
			"ABOUT_APP_EMAIL": "http://www.nmns.edu.tw/web/intro/contact.htm",
			"LEARNING_TABLE_EASY": "基礎學習單",
			"LEARNING_TABLE_MEDIUM": "進階學習單",
			"LEARNING_TABLE_HARD": "挑戰學習單",
			"LEARNING_EASY": "簡易",
			"LEARNING_MEDIUM": "中級",
			"LEARNING_HARD": "挑戰"
		}
	};

/***/ },
/* 148 */
/***/ function(module, exports) {

	module.exports = {
		"menus": [
			{
				"title": "Home",
				"link": "#home"
			},
			{
				"title": "Area",
				"link": "#area",
				"comments": "refers to exhibitions 八大展區 每一個都放一個carousel for items"
			},
			{
				"title": "Game",
				"link": "#game"
			},
			{
				"title": "Voice Tour",
				"link": "#voiceguide",
				"comments": "特別頁製作 輸入號碼 VOICE MP3"
			},
			{
				"title": "Learning",
				"link": "#learn",
				"comments": "學習單列表 下載"
			},
			{
				"title": "Quiz",
				"link": "#quiz"
			},
			{
				"title": "Map",
				"link": "#map",
				"comments": "文字+圖"
			},
			{
				"title": "Transportation",
				"link": "#transportation"
			},
			{
				"title": "About",
				"link": "#about",
				"comments": "開放時間+相關連結+FB粉絲團+介紹影片"
			}
		],
		"blocks": [
			{
				"id": "area",
				"title": "Area"
			},
			{
				"id": "game",
				"title": "Games"
			},
			{
				"id": "voiceguide",
				"title": "VoiceTour"
			},
			{
				"id": "learn",
				"title": "Learning"
			},
			{
				"id": "quiz",
				"title": "Quiz"
			},
			{
				"id": "map",
				"title": "Map"
			},
			{
				"id": "transportation",
				"title": "Transportation"
			},
			{
				"id": "about",
				"title": "About"
			}
		],
		"exhibitions": [
			{
				"id": "1",
				"title": "Introduction",
				"image": "static/img/exhibit/01.jpg"
			},
			{
				"id": "2",
				"title": "Electromagnetism",
				"description": "English Description of Electromagnetism ",
				"image": "static/img/exhibit/02.jpg"
			},
			{
				"id": "3",
				"title": "Kinematics",
				"description": "Dynamics is a branch of classical mechanics, mainly to study the changes in the movement and the factors that cause this change, pure description of the movement of objects, do not take into account the factors that lead to movement.\nThe basic law of dynamics is the Newton law of motion proposed by Isaac Newton: the first law (the law of inertia), the second law (the law of acceleration), the third law (the law of force and reaction), for any physical system, As long as we know the nature of its force, we can study the influence of this force on the physical system by referring to Newton's law of motion.",
				"image": "static/img/exhibit/03.jpg"
			},
			{
				"id": "4",
				"title": "Mechanics",
				"description": "Mechanics is a branch of physics that focuses on the relationship between energy and force and their balance, deformation, or movement with objects. Galileo's law of free-fall movement, and Newton's law of motion, laid the foundations of dynamics. Mechanics can be divided into classical mechanics and quantum mechanics, classical mechanics mainly study low-speed or static macro objects, to explore the movement of the material and the interaction between each other, as small as the point of mass, to the galaxy, the universe; quantum mechanics wider range of applications , Mainly to describe the microscopic things (molecules, atoms, subatomic particles) theory.",
				"image": "static/img/exhibit/04.jpg"
			},
			{
				"id": "5",
				"title": "Fluid mechanics",
				"description": "Fluid mechanics is a branch of mechanics, is to study the fluid (including gas, liquid and plasma) phenomenon and related mechanical behavior of science. Hydrodynamics can be divided into hydrodynamics and hydrodynamics according to the movement of the subject. The former studies the fluid in a quiescent state, and the latter studies the effect on fluid motion. Fluid mechanics in accordance with the scope of application, divided into: air mechanics and hydraulics and so on.",
				"image": "static/img/exhibit/05.jpg"
			},
			{
				"id": "6",
				"title": "Fluctuation",
				"description": "Fluctuation is a physical phenomenon of spatially propagated, and the speed of wave propagation is always limited. Fluctuation is usually a behavior that conveys energy by media (the behavior of a group of particles), such as sonic waves, but some fluctuations do not require medium to transfer energy, such as light waves. The velocity of the wave is related to the inertia of the medium, but not to the nature of the wave source. Wave according to the number of vibration sources can be divided into pulse and periodic wave. When the waves are transmitted in a uniform, non-directional medium, the vibrational direction of the medium can be divided into longitudinal and shear waves.",
				"image": "static/img/exhibit/06.jpg"
			},
			{
				"id": "7",
				"title": "Modern physics",
				"description": "The twentieth century is the great discovery of physics, but also the physical concept of the great revolutionary era. During this period the development of physics is called modern physics, and before that physics is called classical physics. \"X-ray\", \"natural radiation\", \"electronic\" three major physical discovery, opened the 20th century physics discovery era prelude. Quantum mechanics and relativity form two important cornerstones of modern physics.",
				"image": "static/img/exhibit/07.jpg"
			},
			{
				"id": "8",
				"title": "Optics",
				"description": "Optics is mainly to study the phenomenon of light, nature and application, including the interaction between light and matter, the production of optical instruments. Optics usually studies the physical behavior of infrared, ultraviolet and visible light. Because light is electromagnetic waves, other forms of electromagnetic radiation, such as X-ray, microwave, electromagnetic radiation and radio waves, also have similar light characteristics. Which geometric optics, describes the propagation of light, a lens, mirror and prism combination of the optical system, with geometric optics can explain the reflection, refraction and so on.",
				"image": "static/img/exhibit/08.jpg"
			}
		],
		"items": [
			{
				"id": "1",
				"iid": "1-1",
				"category": "序展",
				"title": "Physics Gate",
				"title_en": "Physics Gate",
				"description": "Physics Gate Description",
				"image": "static/img/exhibit/1-1.jpg",
				"video": "https://www.youtube.com/embed/Jj9p3Yjklvw"
			},
			{
				"id": "1",
				"iid": "1-2",
				"category": "序展",
				"title": "Headquarter:Physics Tutor",
				"title_en": "Headquarter:Physics Tutor",
				"description": "Description",
				"image": "static/img/exhibit/1-2.jpg"
			},
			{
				"id": "1",
				"iid": "1-3",
				"category": "序展",
				"title": "Interactive Learning Desk (Back End LearningSystem)",
				"title_en": "Interactive Learning Desk (Back End LearningSystem)",
				"description": "Description",
				"image": "static/img/exhibit/1-3.jpg"
			},
			{
				"id": "1",
				"iid": "1-5",
				"category": "序展",
				"title": "Fortress of Spirit",
				"title_en": "Fortress of Spirit",
				"description": "Description",
				"image": "static/img/exhibit/1-5.jpg"
			},
			{
				"id": "2",
				"iid": "2-1",
				"category": "電磁學",
				"title": "Artificial Magnetic Cloud",
				"title_en": "Artificial Magnetic Cloud",
				"description": "Hold the magnet and slowly draw the iron dust from the bottom of the container from outside, and then, move the magnet away. The iron dust will fall and form a dream-like \"magnetic cloud.\" Now, use the magnet to attract the iron dust that is still falling, and you will see the iron dust start to gather again. When you look at the iron dust from across, you will find it align and stretch towards the outside in a distinctive pattern of lines. What are these lines?",
				"image": "static/img/exhibit/2-1.jpg"
			},
			{
				"id": "2",
				"iid": "2-2",
				"category": "電磁學",
				"title": "Magnetic Property of Matter",
				"title_en": "Magnetic Property of Matter",
				"description": "Take up the sticks in turn. Keep each stick level and pass it through the magnetic arch from top to bottom. Which stick will be pulled by this externally applied magnetic field? When the pulling force appears, is it the force of attraction or repulsion? Why? Is the stick itself magnetic when it is pulled? After trying every stick, compare the pulling force and its strength of the sticks of different materials. It helps you to understand the magnetism of matter. After viewing the scientific explanation on the screen next to the device, think about it and see if you know what matter the magnetic arch contains. What kind of magnetism it has? ",
				"image": "static/img/exhibit/2-2.jpg"
			},
			{
				"id": "2",
				"iid": "2-3",
				"category": "電磁學",
				"title": "Magnetic Force - Attractive or Repulsive ",
				"title_en": "Magnetic Force - Attractive or Repulsive ",
				"description": "The magnet at the center of the ring is attracted to the magnet outside, but the six small magnets around the ring are repelled by the external magnet. With this combination, what will happen when you bring the ring closer to the external magnet? Will they be attracted to or repelled by each other? What will happen when you bring them closer and pull away again? If you do not bring them closer on the same plane but on top of each other, what phenomenon can you observe? Will it be drawn in or bounced away? What will happen if you turn this vertical device upside down? Will the externally applied magnet fall or not?  ",
				"image": "static/img/exhibit/2-3.jpg"
			},
			{
				"id": "2",
				"iid": "2-4",
				"category": "電磁學",
				"title": "Collision that Violates Energy Conservation？",
				"title_en": "Collision that Violates Energy Conservation？",
				"description": "Press the button and watch the ball slowly rolls down from the platform and collides into a string of five balls. The last ball gains the energy from the collision, and unexpectedly jumps to a higher position than the small ball that rolls down! This implies that it gains more potential energy. However, is this not violating the law of energy conservation? Here is your chance of play the observant Sherlock Holms, and see if you can find out the secret of this process that clearly violates the law of conservation of energy. ",
				"image": "static/img/exhibit/2-4.jpg"
			},
			{
				"id": "2",
				"iid": "2-5",
				"category": "電磁學",
				"title": "Magnetic Force against Gravitational Force",
				"title_en": "Magnetic Force against Gravitational Force",
				"description": "Take up the spiral metal on the table and try to attract the ball in the box. Bring the ball onto the top of each tube and let it fall into the tubes in turn. Observe whether it takes the same amount of time for the ball to fall out of each tube. If not, which tube takes the shortest time? Which one takes the longest? Why? If you are tall enough, after putting the ball into the tube, look into the tube from its top and observe the ball's slow but even falling process. ",
				"image": "static/img/exhibit/2-5.jpg"
			},
			{
				"id": "2",
				"iid": "2-6",
				"category": "電磁學",
				"title": "The Invisible Attractive Force",
				"title_en": "The Invisible Attractive Force",
				"description": "Press the button to electrify the coil. Then, insert sticks of different materials in turn into the center of the coil, and feel which stick is pulled or pushed. Is it attraction or repulsion? Why? If the coil is not electrified, can you observe the same phenomenon? Compare the material of each stick, and you will find out the magnetic characteristics of different materials. ",
				"image": "static/img/exhibit/2-6.jpg"
			},
			{
				"id": "3",
				"iid": "3-1",
				"category": "運動學",
				"title": "Long-Lasting Wheels of Wind and Fire",
				"title_en": "Long-Lasting Wheels of Wind and Fire",
				"description": "There is a fast rotating disc at the center of the table. Try roll a ball from one end of the table to the other. What rolling track will the ball create? When you place the ball at the center of the disc, as it gradually moves away from the center, what changes will occur to its \"orbital\" and \"spinning\" motion? Why? Try and place the discs or rings on the rotating disc. Can you find a method to make these objects spin for a long time? Roll the discs or rings in an opposite direction of the rotating disc. Does this method help? ",
				"image": "static/img/exhibit/3-1.jpg"
			},
			{
				"id": "3",
				"iid": "3-2",
				"category": "運動學",
				"title": "Moment of Inertia and Rotational Energy",
				"title_en": "Moment of Inertia and Rotational Energy",
				"description": "The handles on both sides of the disc can be turned in a reversed direction to adjust the placement of weights, which will alter the mass distribution in the entire disc. Move the weights of one disc to its center, and distribute the weights of another disc to its rim. Release the two discs simultaneously from the top of the slide. Which one will arrive at the end first? Why? ",
				"image": "static/img/exhibit/3-2.jpg"
			},
			{
				"id": "3",
				"iid": "3-3",
				"category": "運動學",
				"title": "A Sphere, a Disc, and A Ring, which one runs the fastest?",
				"title_en": "A Sphere, a Disc, and A Ring, which one runs the fastest?",
				"description": "The mass of the ball, disc, and ring is the same. When they are released and rolled down from the same height, which one will arrive at the end first? Why? ",
				"image": "static/img/exhibit/3-3.jpg"
			},
			{
				"id": "3",
				"iid": "3-4",
				"category": "運動學",
				"title": "Rolling Ball Relay",
				"title_en": "Rolling Ball Relay",
				"description": "Line up the magnetic rails in different ways on the wall to form a downward trail. Whose trail can keep the ball rolling for the longest? What energy conversion between two forms of energy does this exhibit show?",
				"image": "static/img/exhibit/3-4.jpg"
			},
			{
				"id": "4",
				"iid": "4-1",
				"category": "力學",
				"title": "Grandiose Coffee Cup of Coriolis Force",
				"title_en": "Grandiose Coffee Cup of Coriolis Force",
				"description": "Follow the instructions of our onsite instructor and sit tight in the coffee cups. Before the coffee cups begin to spin, first observe the linear rolling track of the ball on the ground. Then, when the coffee cups are spinning steadily, observe the rolling track of the ball again. Is there any difference? If yes, is the change real or unreal? Before the coffee cup begins to spin, the instructor will start the \"Foucault Pendulum\" hanging at the center. After the spinning begins, is there any change taking place on the undulating plane? If yes, is the change real or unreal? These changes are all due to the \"Coriolis Force.\" Is the \"Coriolis Force\" a real force? After leaving the coffee cup, watch the quad screen outside the railing, and observe the differences between a \"rotating coordinate system\" and a \"stationary coordinate system\" from the ball's rolling tracks or the pendulum's undulating plane. ",
				"image": "static/img/exhibit/4-1.jpg"
			},
			{
				"id": "4",
				"iid": "4-1",
				"title": "Power Feedback System",
				"title_en": "Power Feedback System",
				"description": "This exhibit is installed with a \"power feedback system,\" which re-harvests the energy of braking and is provided by Delta Electronics. Power equipment in the past uses braking resistor to decrease energy and achieve the purpose of deceleration. However, this process not only waste energy but also causes heat. The new system of Delta Electronics transforms the motor into a generator during the braking process, and converts the braking energy into electricity, which is transmitted back to the electric system for reuse, achieving the results of conserving energy and benefiting the environment. ",
				"image": "static/img/exhibit/4-1.jpg"
			},
			{
				"id": "4",
				"iid": "4-2",
				"category": "力學",
				"title": "Faithful Newton's Cradle",
				"title_en": "Faithful Newton's Cradle",
				"description": "Newton's Cradle looks rather simple, just five balls colliding with each other. However, it can be displayed in various ways to test one's understanding of \"energy\" and \"momentum.\" Pull up a ball and let it collide with the rest. Does it knock one ball flying or all four of them? Why? Pull up two balls and let them collide with the rest. What will happen, then? You will really need to think about it if you pull up three or four balls! In addition, what will happen when you pull up two balls from both sides and let them collide? Will they bounce back? If they do, is it because the balls have \"smashed into a wall,\" or is it because of energy exchange between the balls from the right and the ones from the left? How will you test to find out the answer? The five balls on the right are of identical but lighter mass, whereas the ones on the left are of non-identical mass. What results will you get if you repeat the process mentioned above in these systems? ",
				"image": "static/img/exhibit/4-2.jpg"
			},
			{
				"id": "4",
				"iid": "4-3",
				"category": "力學",
				"title": "Capricious Chaotic Pendulum",
				"title_en": "Capricious Chaotic Pendulum",
				"description": "Turn the knob at the center to adjust the lower T-shaped arm into an upward, vertical position. When all the short arms are stationary, loosen the knob to let the arm start falling and rotating. Repeat this process for a few times. When repeating, try to make the initial condition as identical as possible and see if the following swinging will be identical. You can also start from a different angle and observe whether the subsequent movement will be the same when the initial condition maintains almost the same. Is there any slight difference between the initial condition of each experiment? ",
				"image": "static/img/exhibit/4-3.jpg"
			},
			{
				"id": "5",
				"iid": "5-1",
				"category": "流體力學",
				"title": "Tornado in Water",
				"title_en": "Tornado in Water",
				"description": "Press the button on the right and observe the change of the water above when the shell below starts to spin in high speed. When a vortex appears, does it grow from the top down or does it develop from the bottom up? Press the button on the left, when the short-paddle propeller starts to push the water in high speed, observe the change on the water surface above. What will happen when you press the button for a second time? Why is there a large amount of air bubbles? In the two cylinders, what physical phenomenon is it when the vortex forms from the top down? ",
				"image": "static/img/exhibit/5-1.jpg"
			},
			{
				"id": "5",
				"iid": "5-2",
				"category": "流體力學",
				"title": "Secrets of Green Building",
				"title_en": "Secrets of Green Building",
				"description": "In Taiwan, there are more than six months a year when the weather is warm to people. Consequently, the demand of air-conditioning has used up a massive amount of energy. Let's think about how a building can conserve energy and reduce carbon emission in terms of air-conditioning. The Magic School of Green Technologies at National Cheng Kung University is the first \"green building of zero carbon emission\" in Taiwan. Let's explore the secrets of green architecture now! Adopting hydromechanics to simulate the convection of cool and hot air, in which hot air ascends while cool air descends, the design contains a low air inlet and a high air outlet. Hot air will go up and be let out due to the chimney effect, and consequently let in cool air from the openings of the building. Buoyancy-driven ventilation tower adopts the principle of buoyancy-driven ventilation to create an energy-conserving and airy environment. ",
				"image": "static/img/exhibit/5-2.jpg"
			},
			{
				"id": "6",
				"iid": "6-1",
				"category": "波動",
				"title": "Sympathetic Resonance and Robotic Arm",
				"title_en": "Sympathetic Resonance and Robotic Arm",
				"description": "The robotic arm in this exhibit is provided by Delta Electronics. In accordance to the timetable, the mechanical device will initiate automatically. The robotic arm can perform meticulous movements and pick up objects with precision. It will bring each metronome in turn to the spring area, where its spring will be wound up. Then, it will brush through all metronomes to let them all start oscillating. At first, every metronome will oscillate in a different pace. However, after a short time, all metronomes will reach \"sympathetic oscillation,\" creating a very impressive visual scene and audio effect! Observe how long it takes for the metronomes to go from random oscillation to full sympathetic oscillation. How is it that the metronomes can \"communicate with each other\" and eventually reach sympathetic oscillation? What is the secret of this exhibit? ",
				"image": "static/img/exhibit/6-1.jpg"
			},
			{
				"id": "6",
				"iid": "6-2",
				"category": "波動",
				"title": "The Trace of Waves",
				"title_en": "The Trace of Waves",
				"description": "The top spring of this exhibit demonstrates the production and travel of the \"longitudinal wave\" and the low spring that of the \"transverse wave.\" Give the top bar a strong push, you will see uneven but forward-moving longitudinal waves on the spring. If the other end is fixed, what phenomenon will you see when the longitudinal waves from your end hit the fixed end? What changes will occur in the phase of the reflective waves? When visitors push from both ends at the same time, you will see the longitudinal waves from both sides pass through each other. Do any changes occur to the strength and shape of the two groups of longitudinal waves before and after they pass through each other? When the low bar is given a strong push in a horizontal direction, you will see swaying and forward-moving transverse waves on the spring. Repeat the abovementioned one-person and two-person operations, and observe the reflection and penetration of the transverse waves. ",
				"image": "static/img/exhibit/6-2.jpg"
			},
			{
				"id": "6",
				"iid": "6-3",
				"category": "波動",
				"title": "Vertical Snake Pendulum ",
				"title_en": "Vertical Snake Pendulum ",
				"description": "The period of a simple pendulum and the pendulum length is related to a place's acceleration of gravity. In this exhibit, each ball has a different pendulum length, but they are all suspended vertically. Turn the handle to initiate the oscillation of all balls. Because each ball has a different period of oscillation, they will demonstrate different oscillation and wave combination. Operator can take a step back to admire the movement. Think about it. each ball has a fixed oscillation period defined by its pendulum length. Why is it that the wave pattern of the entire snake pendulum keeps changing? This demonstration adopts an artistic approach to express the scientific content!",
				"image": "static/img/exhibit/6-3.jpg"
			},
			{
				"id": "6",
				"iid": "6-4",
				"category": "波動",
				"title": "Horizontal Snake Pendulum",
				"title_en": "Horizontal Snake Pendulum",
				"description": "This snake pendulum is consisted of several horizontally suspended balls. Each ball has a different pendulum length, and hence, a different oscillation period. Turn the operation bar in a counterclockwise direction to initiate the oscillation of the snake pendulum. You can observe the changing waves from both sides. Think about this: each ball has a fixed period of oscillation defined by the length of pendulum. How come the wave pattern of the entire snake pendulum still keeps changing? ",
				"image": "static/img/exhibit/6-4.jpg"
			},
			{
				"id": "7",
				"iid": "7-1",
				"category": "近代物理",
				"title": "The Invisible Energy - Infrared Radiation",
				"title_en": "The Invisible Energy - Infrared Radiation",
				"description": "Three camera capture and project the images of the audience onto three screens in the front. The left is the image of \"visible light,\" the right the \"near infrared\" image, and the center the \"middle infrared\" image. Which wave length is the most familiar to the naked eye? Which wave length needs an external light source so that people in the front will appear? Which wave length is the strongest radiation wave length emanated by the human body? In the middle infrared image in the front, why are there some \"gangster-like\" figures wearing sunglasses? Move an acrylic plate over and see if the middle infrared ray could pass. Light up the three lights on the exhibition panel. The visible light is clear, but which light bulb is missing in the middle infrared image? Why? ",
				"image": "static/img/exhibit/7-1.jpg"
			},
			{
				"id": "7",
				"iid": "7-2",
				"category": "近代物理",
				"title": "A Colorful World of Optical  Spectra",
				"title_en": "A Colorful World of Optical  Spectra",
				"description": "Move the cylindrical spectrometer. Point it to the light sources in the front in turn, and observe the spectra of each light source from the view finder. What differences can you see from the spectra? Some light sources reveal obvious and bright colorful lines while others display a continuum of colors without lines. Some only show a singular color. Where do these differences come from? Observe the shapes and specifications of the light sources. What conclusions can you deduce in terms of the light emission of these sources? ",
				"image": "static/img/exhibit/7-2.jpg"
			},
			{
				"id": "7",
				"iid": "7-3",
				"category": "近代物理",
				"title": "Electro-optic Effect - Little Heroes of Electricity",
				"title_en": "Electro-optic Effect - Little Heroes of Electricity",
				"description": "Please stand on the markers in front of the three screens and wave your hands to start the game and answer the questions on the energy quest! This game is developed by the engineers at Delta Electronics, and allows both one person to experience or two people to collaborate at the same time. For the one-person mode, please choose \"solar power\" or \"thermal power\" to start the game. For the two-person mode, one person will carry out the game while the other operates the hand crank generator, which will light up the light bulb on the left side of the screen and increase the battery charge in the image. ",
				"image": "static/img/exhibit/7-3.jpg"
			},
			{
				"id": "8",
				"iid": "8-1",
				"category": "光學",
				"title": "Magical Optical Aberration",
				"title_en": "Magical Optical Aberration",
				"description": "The concave mirror in front of you will display your \"inverted real image.\" Can this mirror-looking process show our \"erect virtual image,\" though? It seems not! However, when you turn on the camera on your mobile phone and slowly approach the concave mirror, you will see an erect virtual image of yourself on your phone at a certain distance! When you see it, slowly move your mobile phone away, and you will see that the erect virtual image instantly becomes an inverted real image after passing a certain line. Can you infer the focus of this concave mirror by the point where this change occurs? ",
				"image": "static/img/exhibit/8-1.jpg"
			},
			{
				"id": "8",
				"iid": "8-2",
				"category": "光學",
				"title": "A Colorful Table of Geometric Optics",
				"title_en": "A Colorful Table of Geometric Optics",
				"description": "Press the button and you will see multiple lines of colorful laser beams. At the end in the front, smoke begins to be released, allowing you to see the routes of the laser beams. Now you can move the triangular and rectangular prisms and other lenses and mirrors to observe the various effects of geometrical optics, such as reflection, refraction, and diffraction. Think about this: why do you need smoke? What material is this ethereal fog made of? How is it produced? ",
				"image": "static/img/exhibit/8-2.jpg"
			},
			{
				"id": "8",
				"iid": "8-3",
				"category": "光學",
				"title": "Water Running Upward？",
				"title_en": "Water Running Upward？",
				"description": "Everyone knows that water runs from high places to low places. However, in this exhibit, we see that water drops do not fall downward but go upward in the dim light. Why? Does it have anything to do with the flashing light? When we watch TV and movies, sometimes when a horse-drawn carriage moves forward, the wheels spin in a backward direction. It is the same reason with this exhibit: when you see something with a limited time frame and the intermittence is regular, you will see the illusion of something going backward. ",
				"image": "static/img/exhibit/8-3.jpg"
			},
			{
				"id": "8",
				"iid": "8-4",
				"category": "光學",
				"title": "Real Image and Virtual Image",
				"title_en": "Real Image and Virtual Image",
				"description": "Observe the concave mirror in front of you, and see whether your image in the mirror is erect or inverted. Is it a \"real image\" or a \"virtual\" one? Take a flashlight or turn on the light on your mobile phone. Shine the light on your forehead. Can you see a light dot on the forehead of the image? Shine the light on the forehead of the image. Does the same light dot appear on your forehead? The process helps us better understand the meaning of a \"real image.\" Then, turn on the camera on your mobile phone and point it to the center of the concave mirror while slowly approaching it. You will see an erect image of yourself appearing on your phone. Is it a \"real image\" or a \"virtual image\"? Where does the image form? Slowly move your phone away, and you will see the erect image instantaneously becomes an inverted image after passing a certain \"line.\" What is this \"line\"?\r\r",
				"image": "static/img/exhibit/8-4.jpg"
			}
		],
		"game": [
			{
				"id": 1,
				"title": "looking for export - magnetic",
				"description": "Magnet is an object that can attract iron and produce a magnetic field outside it. The magnetic pole pointing to the north is called the north pole or the N pole, and the magnetic pole pointing to the south is the pole or the S pole. Magnet dissimilar is sucked, the same pole is excluded. Magnet is divided into permanent magnets and non-permanent magnets, non-permanent magnets are usually generated in the form of electromagnets, the use of current to strengthen the magnetic field.",
				"image": "static/img/game/1.png"
			},
			{
				"id": 2,
				"title": "Flying Rota - gravitation",
				"description": "Gravity, also known as gravitation, refers to the quality of objects between the role of mutual attraction, but also the source of the weight of the object. The quality of the planet is different, the gravitational force is different, the mass of the moon is only one-sixth of the earth, so gravity is one-sixth of the earth. On the moon to jump hard, you can jump higher than the earth farther.",
				"image": "static/img/game/2.png"
			},
			{
				"id": 3,
				"title": "a cup just - friction",
				"description": "When an object slides or is about to slide on the surface of another object, it will produce a force to prevent relative movement on the contact surface. It is called friction force. The friction force will convert the kinetic energy of the object into heat energy. Friction is from the electromagnetic force between charged particles, including electrons, protons, atoms and molecules between the force.",
				"image": "static/img/game/3.png"
			},
			{
				"id": 4,
				"title": "water pipe Solitaire - fluid mechanics",
				"description": "Fluid mechanics is a branch of mechanics, is to study the fluid (including gas, liquid and plasma) phenomenon and related mechanical behavior of science. The way of fluid movement is influenced by the nature of the fluid, the shape of the flow path, the magnitude of the applied pressure, and the flow and deformation are the most significant properties of the fluid.",
				"image": "static/img/game/4.png"
			},
			{
				"id": 5,
				"title": "demolition brigade - energy conservation",
				"description": "Object (or system) state of motion remains the same, because the energy is conserved, can not be created or eliminated, can only be converted into different forms. For example, when the rocket takes off, a large amount of gas is ejected vertically downward. According to the principle of conservation of momentum, the rocket obtained upward momentum, the size should be just the same momentum with the gas down.",
				"image": "static/img/game/5.png"
			},
			{
				"id": 6,
				"title": "serpent master - simple harmonic movement",
				"description": "The snake is composed of several pendants, and the shape of the traveling wave appears after swinging. The wavelength changes into the state of chaos over time, and can not see the waveform. Then the process of separating the ball from the two balls is as if the direction of the first half cycle is repeated, but the direction of the traveling wave is changed, contrary to the direction of the first half of the cycle, and finally returns to the initial straight line state.",
				"image": "static/img/game/6.png"
			},
			{
				"id": 7,
				"title": "light journey - light",
				"description": "When the light from a medium into another medium, back to the original media light, called the reflection, into another medium, called the refraction. When the light reaches a different medium, such as: smooth mirror, will make most of the light reflected back, parallel to the beam after the smooth mirror is still parallel to the injection.",
				"image": "static/img/game/7.png"
			},
			{
				"id": 8,
				"title": "glory moment",
				"description": "When you pass the seven hurdles, get seven medals, as long as the answer to the last three questions, you can get the ultimate supreme science medal.",
				"image": "static/img/game/8.png"
			}
		],
		"voiceguide": [
			{
				"id": "1-1",
				"category": "序展",
				"title": "Physics Gate",
				"title_en": "Physics Gate"
			},
			{
				"id": "1-2",
				"category": "序展",
				"title": "Headquarter:Physics Tutor",
				"title_en": "Headquarter:Physics Tutor"
			},
			{
				"id": "1-3",
				"category": "序展",
				"title": "Interactive Learning Desk (Back End LearningSystem)",
				"title_en": "Interactive Learning Desk (Back End LearningSystem)"
			},
			{
				"id": "1-4",
				"category": "序展",
				"title": "Physics Plaza",
				"title_en": "Physics Plaza"
			},
			{
				"id": "1-5",
				"category": "序展",
				"title": "Fortress of Spirit",
				"title_en": "Fortress of Spirit"
			},
			{
				"id": "2-1",
				"category": "電磁學",
				"title": "Artificial Magnetic Cloud",
				"title_en": "Artificial Magnetic Cloud",
				"description": "Hold the magnet and slowly draw the iron dust from the bottom of the container from outside, and then, move the magnet away. The iron dust will fall and form a dream-like \"magnetic cloud.\" Now, use the magnet to attract the iron dust that is still falling, and you will see the iron dust start to gather again. When you look at the iron dust from across, you will find it align and stretch towards the outside in a distinctive pattern of lines. What are these lines?"
			},
			{
				"id": "2-2",
				"category": "電磁學",
				"title": "Magnetic Property of Matter",
				"title_en": "Magnetic Property of Matter",
				"description": "Take up the sticks in turn. Keep each stick level and pass it through the magnetic arch from top to bottom. Which stick will be pulled by this externally applied magnetic field? When the pulling force appears, is it the force of attraction or repulsion? Why? Is the stick itself magnetic when it is pulled? After trying every stick, compare the pulling force and its strength of the sticks of different materials. It helps you to understand the magnetism of matter. After viewing the scientific explanation on the screen next to the device, think about it and see if you know what matter the magnetic arch contains. What kind of magnetism it has? "
			},
			{
				"id": "2-3",
				"category": "電磁學",
				"title": "Magnetic Force - Attractive or Repulsive ",
				"title_en": "Magnetic Force - Attractive or Repulsive ",
				"description": "The magnet at the center of the ring is attracted to the magnet outside, but the six small magnets around the ring are repelled by the external magnet. With this combination, what will happen when you bring the ring closer to the external magnet? Will they be attracted to or repelled by each other? What will happen when you bring them closer and pull away again? If you do not bring them closer on the same plane but on top of each other, what phenomenon can you observe? Will it be drawn in or bounced away? What will happen if you turn this vertical device upside down? Will the externally applied magnet fall or not?  "
			},
			{
				"id": "2-4",
				"category": "電磁學",
				"title": "Collision that Violates Energy Conservation？",
				"title_en": "Collision that Violates Energy Conservation？",
				"description": "Press the button and watch the ball slowly rolls down from the platform and collides into a string of five balls. The last ball gains the energy from the collision, and unexpectedly jumps to a higher position than the small ball that rolls down! This implies that it gains more potential energy. However, is this not violating the law of energy conservation? Here is your chance of play the observant Sherlock Holms, and see if you can find out the secret of this process that clearly violates the law of conservation of energy. "
			},
			{
				"id": "2-5",
				"category": "電磁學",
				"title": "Magnetic Force against Gravitational Force",
				"title_en": "Magnetic Force against Gravitational Force",
				"description": "Take up the spiral metal on the table and try to attract the ball in the box. Bring the ball onto the top of each tube and let it fall into the tubes in turn. Observe whether it takes the same amount of time for the ball to fall out of each tube. If not, which tube takes the shortest time? Which one takes the longest? Why? If you are tall enough, after putting the ball into the tube, look into the tube from its top and observe the ball's slow but even falling process. "
			},
			{
				"id": "2-6",
				"category": "電磁學",
				"title": "The Invisible Attractive Force",
				"title_en": "The Invisible Attractive Force",
				"description": "Press the button to electrify the coil. Then, insert sticks of different materials in turn into the center of the coil, and feel which stick is pulled or pushed. Is it attraction or repulsion? Why? If the coil is not electrified, can you observe the same phenomenon? Compare the material of each stick, and you will find out the magnetic characteristics of different materials. "
			},
			{
				"id": "3-1",
				"category": "運動學",
				"title": "Long-Lasting Wheels of Wind and Fire",
				"title_en": "Long-Lasting Wheels of Wind and Fire",
				"description": "There is a fast rotating disc at the center of the table. Try roll a ball from one end of the table to the other. What rolling track will the ball create? When you place the ball at the center of the disc, as it gradually moves away from the center, what changes will occur to its \"orbital\" and \"spinning\" motion? Why? Try and place the discs or rings on the rotating disc. Can you find a method to make these objects spin for a long time? Roll the discs or rings in an opposite direction of the rotating disc. Does this method help? "
			},
			{
				"id": "3-2",
				"category": "運動學",
				"title": "Moment of Inertia and Rotational Energy",
				"title_en": "Moment of Inertia and Rotational Energy",
				"description": "The handles on both sides of the disc can be turned in a reversed direction to adjust the placement of weights, which will alter the mass distribution in the entire disc. Move the weights of one disc to its center, and distribute the weights of another disc to its rim. Release the two discs simultaneously from the top of the slide. Which one will arrive at the end first? Why? "
			},
			{
				"id": "3-3",
				"category": "運動學",
				"title": "A Sphere, a Disc, and A Ring, which one runs the fastest?",
				"title_en": "A Sphere, a Disc, and A Ring, which one runs the fastest?",
				"description": "The mass of the ball, disc, and ring is the same. When they are released and rolled down from the same height, which one will arrive at the end first? Why? "
			},
			{
				"id": "3-4",
				"category": "運動學",
				"title": "Rolling Ball Relay",
				"title_en": "Rolling Ball Relay",
				"description": "Line up the magnetic rails in different ways on the wall to form a downward trail. Whose trail can keep the ball rolling for the longest? What energy conversion between two forms of energy does this exhibit show?"
			},
			{
				"id": "4-1",
				"category": "力學",
				"title": "Grandiose Coffee Cup of Coriolis Force",
				"title_en": "Grandiose Coffee Cup of Coriolis Force",
				"description": "Follow the instructions of our onsite instructor and sit tight in the coffee cups. Before the coffee cups begin to spin, first observe the linear rolling track of the ball on the ground. Then, when the coffee cups are spinning steadily, observe the rolling track of the ball again. Is there any difference? If yes, is the change real or unreal? Before the coffee cup begins to spin, the instructor will start the \"Foucault Pendulum\" hanging at the center. After the spinning begins, is there any change taking place on the undulating plane? If yes, is the change real or unreal? These changes are all due to the \"Coriolis Force.\" Is the \"Coriolis Force\" a real force? After leaving the coffee cup, watch the quad screen outside the railing, and observe the differences between a \"rotating coordinate system\" and a \"stationary coordinate system\" from the ball's rolling tracks or the pendulum's undulating plane. "
			},
			{
				"id": "4-2",
				"category": "力學",
				"title": "Faithful Newton's Cradle",
				"title_en": "Faithful Newton's Cradle",
				"description": "Newton's Cradle looks rather simple, just five balls colliding with each other. However, it can be displayed in various ways to test one's understanding of \"energy\" and \"momentum.\" Pull up a ball and let it collide with the rest. Does it knock one ball flying or all four of them? Why? Pull up two balls and let them collide with the rest. What will happen, then? You will really need to think about it if you pull up three or four balls! In addition, what will happen when you pull up two balls from both sides and let them collide? Will they bounce back? If they do, is it because the balls have \"smashed into a wall,\" or is it because of energy exchange between the balls from the right and the ones from the left? How will you test to find out the answer? The five balls on the right are of identical but lighter mass, whereas the ones on the left are of non-identical mass. What results will you get if you repeat the process mentioned above in these systems? "
			},
			{
				"id": "4-3",
				"category": "力學",
				"title": "Capricious Chaotic Pendulum",
				"title_en": "Capricious Chaotic Pendulum",
				"description": "Turn the knob at the center to adjust the lower T-shaped arm into an upward, vertical position. When all the short arms are stationary, loosen the knob to let the arm start falling and rotating. Repeat this process for a few times. When repeating, try to make the initial condition as identical as possible and see if the following swinging will be identical. You can also start from a different angle and observe whether the subsequent movement will be the same when the initial condition maintains almost the same. Is there any slight difference between the initial condition of each experiment? "
			},
			{
				"id": "5-1",
				"category": "流體力學",
				"title": "Tornado in Water",
				"title_en": "Tornado in Water",
				"description": "Press the button on the right and observe the change of the water above when the shell below starts to spin in high speed. When a vortex appears, does it grow from the top down or does it develop from the bottom up? Press the button on the left, when the short-paddle propeller starts to push the water in high speed, observe the change on the water surface above. What will happen when you press the button for a second time? Why is there a large amount of air bubbles? In the two cylinders, what physical phenomenon is it when the vortex forms from the top down? "
			},
			{
				"id": "5-2",
				"category": "流體力學",
				"title": "Secrets of Green Building",
				"title_en": "Secrets of Green Building",
				"description": "In Taiwan, there are more than six months a year when the weather is warm to people. Consequently, the demand of air-conditioning has used up a massive amount of energy. Let's think about how a building can conserve energy and reduce carbon emission in terms of air-conditioning. The Magic School of Green Technologies at National Cheng Kung University is the first \"green building of zero carbon emission\" in Taiwan. Let's explore the secrets of green architecture now! Adopting hydromechanics to simulate the convection of cool and hot air, in which hot air ascends while cool air descends, the design contains a low air inlet and a high air outlet. Hot air will go up and be let out due to the chimney effect, and consequently let in cool air from the openings of the building. Buoyancy-driven ventilation tower adopts the principle of buoyancy-driven ventilation to create an energy-conserving and airy environment. "
			},
			{
				"id": "6-1",
				"category": "波動",
				"title": "Sympathetic Resonance and Robotic Arm",
				"title_en": "Sympathetic Resonance and Robotic Arm",
				"description": "The robotic arm in this exhibit is provided by Delta Electronics. In accordance to the timetable, the mechanical device will initiate automatically. The robotic arm can perform meticulous movements and pick up objects with precision. It will bring each metronome in turn to the spring area, where its spring will be wound up. Then, it will brush through all metronomes to let them all start oscillating. At first, every metronome will oscillate in a different pace. However, after a short time, all metronomes will reach \"sympathetic oscillation,\" creating a very impressive visual scene and audio effect! Observe how long it takes for the metronomes to go from random oscillation to full sympathetic oscillation. How is it that the metronomes can \"communicate with each other\" and eventually reach sympathetic oscillation? What is the secret of this exhibit? "
			},
			{
				"id": "6-2",
				"category": "波動",
				"title": "The Trace of Waves",
				"title_en": "The Trace of Waves",
				"description": "The top spring of this exhibit demonstrates the production and travel of the \"longitudinal wave\" and the low spring that of the \"transverse wave.\" Give the top bar a strong push, you will see uneven but forward-moving longitudinal waves on the spring. If the other end is fixed, what phenomenon will you see when the longitudinal waves from your end hit the fixed end? What changes will occur in the phase of the reflective waves? When visitors push from both ends at the same time, you will see the longitudinal waves from both sides pass through each other. Do any changes occur to the strength and shape of the two groups of longitudinal waves before and after they pass through each other? When the low bar is given a strong push in a horizontal direction, you will see swaying and forward-moving transverse waves on the spring. Repeat the abovementioned one-person and two-person operations, and observe the reflection and penetration of the transverse waves. "
			},
			{
				"id": "6-3",
				"category": "波動",
				"title": "Vertical Snake Pendulum ",
				"title_en": "Vertical Snake Pendulum ",
				"description": "The period of a simple pendulum and the pendulum length is related to a place's acceleration of gravity. In this exhibit, each ball has a different pendulum length, but they are all suspended vertically. Turn the handle to initiate the oscillation of all balls. Because each ball has a different period of oscillation, they will demonstrate different oscillation and wave combination. Operator can take a step back to admire the movement. Think about it. each ball has a fixed oscillation period defined by its pendulum length. Why is it that the wave pattern of the entire snake pendulum keeps changing? This demonstration adopts an artistic approach to express the scientific content!"
			},
			{
				"id": "6-4",
				"category": "波動",
				"title": "Horizontal Snake Pendulum",
				"title_en": "Horizontal Snake Pendulum",
				"description": "This snake pendulum is consisted of several horizontally suspended balls. Each ball has a different pendulum length, and hence, a different oscillation period. Turn the operation bar in a counterclockwise direction to initiate the oscillation of the snake pendulum. You can observe the changing waves from both sides. Think about this: each ball has a fixed period of oscillation defined by the length of pendulum. How come the wave pattern of the entire snake pendulum still keeps changing? "
			},
			{
				"id": "7-1",
				"category": "近代物理",
				"title": "The Invisible Energy - Infrared Radiation",
				"title_en": "The Invisible Energy - Infrared Radiation",
				"description": "Three camera capture and project the images of the audience onto three screens in the front. The left is the image of \"visible light,\" the right the \"near infrared\" image, and the center the \"middle infrared\" image. Which wave length is the most familiar to the naked eye? Which wave length needs an external light source so that people in the front will appear? Which wave length is the strongest radiation wave length emanated by the human body? In the middle infrared image in the front, why are there some \"gangster-like\" figures wearing sunglasses? Move an acrylic plate over and see if the middle infrared ray could pass. Light up the three lights on the exhibition panel. The visible light is clear, but which light bulb is missing in the middle infrared image? Why? "
			},
			{
				"id": "7-2",
				"category": "近代物理",
				"title": "A Colorful World of Optical  Spectra",
				"title_en": "A Colorful World of Optical  Spectra",
				"description": "Move the cylindrical spectrometer. Point it to the light sources in the front in turn, and observe the spectra of each light source from the view finder. What differences can you see from the spectra? Some light sources reveal obvious and bright colorful lines while others display a continuum of colors without lines. Some only show a singular color. Where do these differences come from? Observe the shapes and specifications of the light sources. What conclusions can you deduce in terms of the light emission of these sources? "
			},
			{
				"id": "7-3",
				"category": "近代物理",
				"title": "Electro-optic Effect - Little Heroes of Electricity",
				"title_en": "Electro-optic Effect - Little Heroes of Electricity",
				"description": "Please stand on the markers in front of the three screens and wave your hands to start the game and answer the questions on the energy quest! This game is developed by the engineers at Delta Electronics, and allows both one person to experience or two people to collaborate at the same time. For the one-person mode, please choose \"solar power\" or \"thermal power\" to start the game. For the two-person mode, one person will carry out the game while the other operates the hand crank generator, which will light up the light bulb on the left side of the screen and increase the battery charge in the image. "
			},
			{
				"id": "8-1",
				"category": "光學",
				"title": "Magical Optical Aberration",
				"title_en": "Magical Optical Aberration",
				"description": "The concave mirror in front of you will display your \"inverted real image.\" Can this mirror-looking process show our \"erect virtual image,\" though? It seems not! However, when you turn on the camera on your mobile phone and slowly approach the concave mirror, you will see an erect virtual image of yourself on your phone at a certain distance! When you see it, slowly move your mobile phone away, and you will see that the erect virtual image instantly becomes an inverted real image after passing a certain line. Can you infer the focus of this concave mirror by the point where this change occurs? "
			},
			{
				"id": "8-2",
				"category": "光學",
				"title": "A Colorful Table of Geometric Optics",
				"title_en": "A Colorful Table of Geometric Optics",
				"description": "Press the button and you will see multiple lines of colorful laser beams. At the end in the front, smoke begins to be released, allowing you to see the routes of the laser beams. Now you can move the triangular and rectangular prisms and other lenses and mirrors to observe the various effects of geometrical optics, such as reflection, refraction, and diffraction. Think about this: why do you need smoke? What material is this ethereal fog made of? How is it produced? "
			},
			{
				"id": "8-3",
				"category": "光學",
				"title": "Water Running Upward？",
				"title_en": "Water Running Upward？",
				"description": "Everyone knows that water runs from high places to low places. However, in this exhibit, we see that water drops do not fall downward but go upward in the dim light. Why? Does it have anything to do with the flashing light? When we watch TV and movies, sometimes when a horse-drawn carriage moves forward, the wheels spin in a backward direction. It is the same reason with this exhibit: when you see something with a limited time frame and the intermittence is regular, you will see the illusion of something going backward. "
			},
			{
				"id": "8-4",
				"category": "光學",
				"title": "Real Image and Virtual Image",
				"title_en": "Real Image and Virtual Image",
				"description": "Observe the concave mirror in front of you, and see whether your image in the mirror is erect or inverted. Is it a \"real image\" or a \"virtual\" one? Take a flashlight or turn on the light on your mobile phone. Shine the light on your forehead. Can you see a light dot on the forehead of the image? Shine the light on the forehead of the image. Does the same light dot appear on your forehead? The process helps us better understand the meaning of a \"real image.\" Then, turn on the camera on your mobile phone and point it to the center of the concave mirror while slowly approaching it. You will see an erect image of yourself appearing on your phone. Is it a \"real image\" or a \"virtual image\"? Where does the image form? Slowly move your phone away, and you will see the erect image instantaneously becomes an inverted image after passing a certain \"line.\" What is this \"line\"?\r\r"
			}
		],
		"learning": [
			{
				"filename": "Primary level",
				"size": "167KB",
				"url": "static/learn-sheet/learn-sheet-easy.pdf",
				"date": "2017/2/22"
			},
			{
				"filename": "Middle grade",
				"size": "140KB",
				"url": "static/learn-sheet/learn-sheet-medium.pdf",
				"date": "2017/2/22"
			},
			{
				"filename": "Senior grades",
				"size": "158KB",
				"url": "static/learn-sheet/learn-sheet-difficult.pdf",
				"date": "2017/2/22"
			}
		],
		"youtube": [
			{
				"title": "1. 前言導覽",
				"url": "https://www.youtube.com/watch?v=jnuv4PPyW4Y"
			},
			{
				"title": "2. 人造磁雲",
				"url": "https://www.youtube.com/watch?v=NEkDt5bdbXI"
			},
			{
				"title": "3. 物質的磁性 (強力萬磁王)",
				"url": "https://www.youtube.com/watch?v=moUREtsaNSU"
			},
			{
				"title": "4. 若即若離的磁力 (隔空感應)",
				"url": "https://www.youtube.com/watch?v=qkaIifOjA_8"
			},
			{
				"title": "5. 違反能量守恆的碰撞？ (磁力彈弓)",
				"url": "https://www.youtube.com/watch?v=DzYDg0Qndl4"
			},
			{
				"title": "6. 看不見的吸引力 (感應電棒)",
				"url": "https://www.youtube.com/watch?v=IDpi3uyCDtM"
			},
			{
				"title": "7. 磁力抗拒重力？ (磁力管) ",
				"url": "https://www.youtube.com/watch?v=MKT6kO1JKGo"
			},
			{
				"title": "8. 圓球•圓盤•圓環，誰跑得最快？ (轉動慣量)",
				"url": "https://www.youtube.com/watch?v=_C4uUAta8ZU"
			},
			{
				"title": "9. 永不停歇的風火輪 (滾動競賽）",
				"url": "https://www.youtube.com/watch?v=XYCX0NGqm8c"
			},
			{
				"title": "10. 滾球接龍",
				"url": "https://www.youtube.com/watch?v=IfsW6siwViw"
			},
			{
				"title": "11. 水中龍捲風",
				"url": "https://www.youtube.com/watch?v=VSZPpH1U3bE"
			},
			{
				"title": "12. 忠實的牛頓擺",
				"url": "https://www.youtube.com/watch?v=OFbZW8D4Xcg"
			},
			{
				"title": "13. 空中發射",
				"url": "https://www.youtube.com/watch?v=G3diHFMBo-8"
			},
			{
				"title": "14. 科氏力大型咖啡杯",
				"url": "https://www.youtube.com/watch?v=NzgfwRn0S30"
			},
			{
				"title": "15. 善變的混沌擺",
				"url": "https://www.youtube.com/watch?v=MpPqycPh190"
			},
			{
				"title": "16. 善變的蛇擺",
				"url": "https://www.youtube.com/watch?v=8u1dK-g2Voc"
			},
			{
				"title": "17. 波的足跡",
				"url": "https://www.youtube.com/watch?v=Vwmuh1KxbIc"
			},
			{
				"title": "18. 同步共振與機械手臂",
				"url": "https://www.youtube.com/watch?v=Y7tWuZse4U8"
			},
			{
				"title": "19. 實像與虛像 (神奇焦點)",
				"url": "https://www.youtube.com/watch?v=uE1RRZqaEjo"
			},
			{
				"title": "20. 魔幻像差",
				"url": "https://www.youtube.com/watch?v=EcH0geRMRSs"
			},
			{
				"title": "21. 看不見的能量-紅外線 (光學實驗台-熱顯像儀)",
				"url": "https://www.youtube.com/watch?v=QP_9TEZUQe0"
			},
			{
				"title": "22. 水往高處流？ (水往上流)",
				"url": "https://www.youtube.com/watch?v=qGMxrv60Ahk"
			},
			{
				"title": "23. 繽紛的光譜世界",
				"url": "https://www.youtube.com/watch?v=DPnyUDcAp3M"
			},
			{
				"title": "24. 電光效應-電力小英雄",
				"url": "https://www.youtube.com/watch?v=wOHdVLEQsvc"
			},
			{
				"title": "25. 七彩光學遊戲台",
				"url": "https://www.youtube.com/watch?v=srkmg0ipLI4"
			}
		],
		"source": {
			"TRAFFIC_MAP_URL": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3640.427123971107!2d120.66336231541106!3d24.156748879047417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34693d78069134cb%3A0xf7a65bba8f974f36!2z5ZyL56uL6Ieq54S256eR5a245Y2a54mp6aSo!5e0!3m2!1szh-TW!2stw!4v1489550639390",
			"TRAFFIC_SRC_MAP_URL": "https://www.google.com.tw/maps/place/National+Museum+of+Natural+Science/@24.1544579,120.6640131,16z/data=!4m5!3m4!1s0x34693d78069134cb:0xf7a65bba8f974f36!8m2!3d24.1572334!4d120.6660604?hl=en",
			"MAP_IMG_URL": "static/img/map.png",
			"FOOTER_PRIVACY_URL": "http://www.nmns.edu.tw/ch/index/privacy.htm",
			"ABOUT_RESERVATION_URL": "http://cal.nmns.edu.tw/NMNS_Cal/Detail_Ann.aspx?ANID=12103",
			"FOOTER_REF_HOMEPAGE_URL": "http://www.nmns.edu.tw/",
			"FOOTER_REF_IG_URL": "https://www.instagram.com/nmnstw/",
			"FOOTER_REF_U2_URL": "https://www.youtube.com/user/NMNSTW1",
			"FOOTER_REF_FB_URL": "https://www.facebook.com/COBOFANS",
			"APP_ANDROID_URL": "https://play.google.com/store/apps/details?id=com.nmns.physical_world",
			"APP_IOS_URL": "https://itunes.apple.com/us/app/%E7%89%A9%E7%90%86%E4%B8%96%E7%95%8C/id1219863796"
		},
		"strings": {
			"TOGGLE_LOCALE": "中文",
			"HOME_TITLE1": "Inspiring Curiosity, Exploring Mysteries",
			"HOME_TITLE2": "The best nourishment for science education.",
			"HOME_ACTION_TITLE": "Know more about the Physics Hall",
			"VOICE_TITLE": "Audio guide for Physical World",
			"VOICE_GUIDE": "Audio Guide",
			"VOICE_KEY_CLEAR_BTN": "Clear",
			"VOICE_KEY_CONFIRM_BTN": "Enter",
			"VOICE_KEY_BACK_BTN": "Previous Page",
			"VOICE_KEY_TIP_TXT": "Put in audio guide number and press enter",
			"VOICE_KEY_ERRORMSG_TXT": "Number not available",
			"VOICE_PLAY_BACK_BTN": "Select again",
			"VOICE_PLAY_HOME_BTN": "Back to Home",
			"TRAFFIC_SRC_MAP_TXT": "View a larger map",
			"TRAFFIC_DESCRIPTION": "The National Museum of Natural Science (NMNS) is located on Taiwan Boulevard in Taichung. If by car, take National Highway No. 1 or 3 to Taichung, and take Taiwan Boulevard to the museum. If by public transportation, take the HSR, TRA, or bus to Taichung, and take the city bus to SOGO Department Store or the NMNS Station. Public transportation is recommended.",
			"ABOUT_POS_TITLE": "Exhibition Venue",
			"ABOUT_POS_TXT": "4F, Science Center. No. 1, Guancian Rd., North Dist., Taichung City",
			"ABOUT_TIME_TITLE": "Opening Hours",
			"ABOUT_TIME_TXT": "Tuesdays to Sundays, 9:00 to 17:00\n Closed on Mondays, but open on Mondays during winter and summer vacations.",
			"ABOUT_TICKET_TITLE": "Ticketing Information",
			"ABOUT_TICKET_TXT": "Free entry from today to Dec. 31, 2017 (Science Center)",
			"ABOUT_NOTICE_TITLE": "Notices",
			"ABOUT_NOTICE_TXT": "To maintain the quality and equal opportunity to viewing and experiencing the exhibition, access control is implemented.",
			"ABOUT_NOTICE_RESERVATION_TXT": "Make an Appointment",
			"FOOTER_DESCRIPTION": "No. 1, Guancian Rd., North Dist., Taichung City (40453)\nTel: (04) 2322-6940\nOpening Hours: Tuesdays to Sundays, 9:00 to 17:00\n(Open on Mondays during winter and summer vacations except during the Chinese New Year holiday.)\nLatest update: 2017-2-21",
			"FOOTER_PRIVACY": "Privacy and Security Policy",
			"FOOTER_REF_TITLE": "Related Links",
			"FOOTER_REF_LINK1": "National Museum of Natural Science",
			"FOOTER_REF_LINK2": "NMNS Instagram",
			"FOOTER_REF_LINK3": "NMNS YouTube",
			"FOOTER_REF_LINK4": "NMNS Facebook Fan Page",
			"FOOTER_FOLLOW_TITLE": "Follow Us",
			"FOOTER_COPYRIGHT_ENG": "© Copyright of the National Museum of Natural Science. All Rights Reserved",
			"LEARNING_TABLE_TITLE": "Download Learning Sheet",
			"LEARNING_FILENAME": "File Name",
			"LEARNING_SIZE": "Size",
			"LEARNING_DIFFICULTY": "Difficulty",
			"LEARNING_DOWNLOAD": "Download File",
			"LEARNING_DATE": "Date of Update",
			"YOUTUBE_TABLE_TITLE": "Youtube",
			"YOUTUBE_TITLE": "Video Title",
			"APPLOGIN_TIP": "Fill in the following information, and you can generate a QR code to play games in the exhibition.",
			"APPLOGIN_TIP2": "Use the QR code to play games at the consoles in the exhibition.",
			"APPLOGIN_NAME": "Name",
			"APPLOGIN_EMAIL": "Email",
			"APPLOGIN_GENDER": "Sex",
			"APPLOGIN_MALE": "Male",
			"APPLOGIN_FEMALE": "Female",
			"APPLOGIN_AGE": "Age",
			"APPLOGIN_UNDER12": "Under 12 years old",
			"APPLOGIN_OLDER60": "Above 60 years old",
			"APPLOGIN_REGISTER": "Register",
			"APPLOGIN_RE_REGISTER": "Re-register",
			"APPLOGIN_ERRORMSG_EMAIL": "Please enter a valid email.",
			"APPLOGIN_ERRORMSG_NAME": "Please enter your name.",
			"HOME_APP_BTN_TIP": "Physical World GuideApp",
			"QUIZ_NEXT_QUESTION": "Next Challenge",
			"QUIZ_CORRECT_MSG": "(O) Correct!",
			"QUIZ_INCORRECT_MSG": "(X) Wrong...",
			"MODAL_CLOSE": "Close",
			"LEARNING_TABLE_EASY": "Basic Learning Sheet",
			"LEARNING_TABLE_MEDIUM": "Advanced Learning Sheet",
			"LEARNING_TABLE_HARD": "Challenging Learning Sheet",
			"LEARNING_EASY": "Easy",
			"LEARNING_MEDIUM": "Medium",
			"LEARNING_HARD": "Hard"
		}
	};

/***/ },
/* 149 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(144)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(49),
	  /* template */
	  __webpack_require__(180),
	  /* scopeId */
	  null,
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 150 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(145)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(50),
	  /* template */
	  __webpack_require__(181),
	  /* scopeId */
	  "data-v-c46e467e",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 151 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(139)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(51),
	  /* template */
	  __webpack_require__(175),
	  /* scopeId */
	  "data-v-68940f2d",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 152 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(142)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(52),
	  /* template */
	  __webpack_require__(178),
	  /* scopeId */
	  "data-v-8351c596",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 153 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(131)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(53),
	  /* template */
	  __webpack_require__(167),
	  /* scopeId */
	  "data-v-0e0eec27",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 154 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(146)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(54),
	  /* template */
	  __webpack_require__(182),
	  /* scopeId */
	  "data-v-db519e70",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 155 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(141)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(55),
	  /* template */
	  __webpack_require__(177),
	  /* scopeId */
	  "data-v-71ed4a9e",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 156 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(143)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(56),
	  /* template */
	  __webpack_require__(179),
	  /* scopeId */
	  "data-v-98ecd68c",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 157 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(138)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(57),
	  /* template */
	  __webpack_require__(174),
	  /* scopeId */
	  "data-v-67b1475e",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 158 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(134)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(58),
	  /* template */
	  __webpack_require__(170),
	  /* scopeId */
	  "data-v-272a9772",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 159 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(130)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(59),
	  /* template */
	  __webpack_require__(166),
	  /* scopeId */
	  "data-v-0881f104",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 160 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(135)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(60),
	  /* template */
	  __webpack_require__(171),
	  /* scopeId */
	  "data-v-30d745aa",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 161 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(132)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(61),
	  /* template */
	  __webpack_require__(168),
	  /* scopeId */
	  "data-v-135ff39b",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 162 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(140)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(62),
	  /* template */
	  __webpack_require__(176),
	  /* scopeId */
	  "data-v-6d58d3c4",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 163 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(133)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(63),
	  /* template */
	  __webpack_require__(169),
	  /* scopeId */
	  "data-v-2401c37a",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 164 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(137)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(64),
	  /* template */
	  __webpack_require__(173),
	  /* scopeId */
	  "data-v-661c8217",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 165 */
/***/ function(module, exports, __webpack_require__) {

	
	/* styles */
	__webpack_require__(136)
	
	var Component = __webpack_require__(3)(
	  /* script */
	  __webpack_require__(65),
	  /* template */
	  __webpack_require__(172),
	  /* scopeId */
	  "data-v-332db190",
	  /* cssModules */
	  null
	)
	
	module.exports = Component.exports


/***/ },
/* 166 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "map row bg-primary"
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('h1', {
	    staticClass: "block_text normal-element text-center"
	  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('img', {
	    attrs: {
	      "src": _vm.data.source.MAP_IMG_URL
	    }
	  })])])
	},staticRenderFns: []}

/***/ },
/* 167 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary",
	    attrs: {
	      "desktop-mode": _vm.desktopMode
	    }
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('div', {
	    staticClass: "carousel slide",
	    attrs: {
	      "id": "areaCarousel",
	      "data-ride": "carousel"
	    }
	  }, [_c('h1', {
	    staticClass: "block_text normal-element text-center"
	  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('div', {
	    staticClass: "carousel-inner",
	    attrs: {
	      "role": "listbox"
	    }
	  }, [_vm._l((_vm.data.exhibitions), function(item, index) {
	    return _c('div', {
	      class: ['item', {
	        'active': index === 0
	      }]
	    }, [_c('div', {
	      staticClass: "carousel-image app-element img-responsive",
	      style: ({
	        backgroundImage: 'url(' + item.image + ')'
	      })
	    }), _vm._v(" "), _c('h2', {
	      staticClass: "area-title app-element"
	    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('img', {
	      staticClass: "normal-element img-responsive",
	      attrs: {
	        "src": item.image,
	        "alt": "area-photo"
	      }
	    }), _vm._v(" "), _c('div', {
	      staticClass: "text_block carousel-caption normal-element"
	    }, [_c('h2', {
	      staticClass: "area-title normal-element"
	    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), (_vm.desktopMode) ? _c('div', {
	      staticClass: "text-left description"
	    }, [_vm._v(_vm._s(item.description))]) : _vm._e()])])
	  }), _vm._v(" "), (!_vm.appMode) ? _c('div', {
	    staticClass: "control"
	  }, [_c('a', {
	    staticClass: "left carousel-control",
	    attrs: {
	      "href": "#areaCarousel",
	      "role": "button",
	      "data-slide": "prev"
	    },
	    on: {
	      "click": function($event) {
	        _vm.prev()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-left",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Previous")])]), _vm._v(" "), _c('a', {
	    staticClass: "right carousel-control",
	    attrs: {
	      "href": "#areaCarousel",
	      "role": "button",
	      "data-slide": "next"
	    },
	    on: {
	      "click": function($event) {
	        _vm.next()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-right",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Next")])])]) : _vm._e(), _vm._v(" "), _c('ol', {
	    staticClass: "carousel-indicators"
	  }, _vm._l((_vm.data.exhibitions), function(item, index) {
	    return _c('li', {
	      class: {
	        active: index === 0
	      },
	      attrs: {
	        "data-slide-to": index,
	        "data-target": "#areaCarousel"
	      },
	      on: {
	        "click": function($event) {
	          _vm.choose(index)
	        }
	      }
	    })
	  }))], 2)]), _vm._v(" "), (!_vm.desktopMode) ? _c('div', {
	    staticClass: "description"
	  }, [_vm._v(_vm._s(_vm.data.exhibitions[_vm.current_idx].description))]) : _vm._e(), _vm._v(" "), _c('div', {
	    staticClass: "detail-button-panel"
	  }, [_c('div', {
	    staticClass: "detail-button-container"
	  }, _vm._l((_vm.get_items(_vm.data.exhibitions[_vm.current_idx].id)), function(child_item, index) {
	    return _c('div', {
	      staticClass: "btn btn-sm btn-outline item-button",
	      attrs: {
	        "type": "button"
	      },
	      on: {
	        "click": function($event) {
	          _vm.modal(child_item)
	        }
	      }
	    }, [_vm._v("\n          " + _vm._s(child_item.title) + "\n        ")])
	  }))]), _vm._v(" "), (_vm.appMode) ? _c('div', {
	    staticClass: "control"
	  }, [_c('a', {
	    staticClass: "left carousel-control",
	    attrs: {
	      "href": "#areaCarousel",
	      "role": "button",
	      "data-slide": "prev"
	    },
	    on: {
	      "click": function($event) {
	        _vm.prev()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-left",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Previous")])]), _vm._v(" "), _c('a', {
	    staticClass: "right carousel-control",
	    attrs: {
	      "href": "#areaCarousel",
	      "role": "button",
	      "data-slide": "next"
	    },
	    on: {
	      "click": function($event) {
	        _vm.next()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-right",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Next")])])]) : _vm._e()])])
	},staticRenderFns: []}

/***/ },
/* 168 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary"
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('h1', {
	    staticClass: "block_text text-center"
	  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('div', {
	    staticClass: "row"
	  }, [(_vm.targetQuestion) ? _c('div', {
	    staticClass: "col-md-12 col-centered"
	  }, [_c('div', {
	    staticClass: "row col-md-12 col-sm-12 question"
	  }, [_vm._m(0), _vm._v(" "), _c('h2', {
	    staticClass: "question-text"
	  }, [_vm._v(_vm._s(_vm.targetQuestion.q))])]), _vm._v(" "), _c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-sm-6 answer",
	    on: {
	      "click": function($event) {
	        _vm.answer(1)
	      }
	    }
	  }, [_vm._m(1), _vm._v(" "), _c('h2', {
	    staticClass: "answer-text"
	  }, [_vm._v(_vm._s(_vm.targetQuestion.a1))])]), _vm._v(" "), _c('div', {
	    staticClass: "col-sm-6 answer",
	    on: {
	      "click": function($event) {
	        _vm.answer(2)
	      }
	    }
	  }, [_vm._m(2), _vm._v(" "), _c('h2', {
	    staticClass: "answer-text"
	  }, [_vm._v(_vm._s(_vm.targetQuestion.a2))])])])]) : _vm._e()])]), _vm._v(" "), _c('div', {
	    directives: [{
	      name: "show",
	      rawName: "v-show",
	      value: (_vm.isDialogVisible),
	      expression: "isDialogVisible"
	    }],
	    staticClass: "dialog"
	  }, [_c('div', {
	    staticClass: "row fullwidth-row txt-row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 banner-text fullwidth-col text-center"
	  }, [_c('h1', {
	    staticClass: "white"
	  }, [_vm._v(_vm._s(_vm.dialogText))]), _vm._v(" "), _c('a', {
	    staticClass: "btn btn-appoint",
	    on: {
	      "click": function($event) {
	        _vm.hideDialog()
	      }
	    }
	  }, [_vm._v(_vm._s(this.l10n('QUIZ_NEXT_QUESTION')))])])])])])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "header"
	  }, [_c('div', {
	    staticClass: "header-background"
	  }), _vm._v(" "), _c('h1', {
	    staticClass: "header-text"
	  }, [_vm._v("Q")])])
	},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "header"
	  }, [_c('div', {
	    staticClass: "header-background"
	  }), _vm._v(" "), _c('h1', {
	    staticClass: "header-text"
	  }, [_vm._v("1")])])
	},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "header"
	  }, [_c('div', {
	    staticClass: "header-background"
	  }), _vm._v(" "), _c('h1', {
	    staticClass: "header-text"
	  }, [_vm._v("2")])])
	}]}

/***/ },
/* 169 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary"
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('h1', {
	    staticClass: "block_text normal-element text-center"
	  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('div', {
	    staticClass: "transportation-description"
	  }, [_vm._v(_vm._s(_vm.data.strings.TRAFFIC_DESCRIPTION))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('iframe', {
	    attrs: {
	      "title": "Google Map--科博館交通圖",
	      "src": _vm.data.source.TRAFFIC_MAP_URL,
	      "frameborder": "0",
	      "width": "100%",
	      "height": "400",
	      "marginwidth": "0",
	      "marginheight": "0",
	      "scrolling": "no"
	    }
	  }), _vm._v(" "), _c('br'), _vm._v(" "), _c('br'), _vm._v(" "), _c('a', {
	    staticClass: "link",
	    attrs: {
	      "href": _vm.data.source.TRAFFIC_SRC_MAP_URL,
	      "target": "_blank"
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.TRAFFIC_SRC_MAP_TXT))])], 1)])
	},staticRenderFns: []}

/***/ },
/* 170 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary"
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('h1', {
	    staticClass: "block_text normal-element text-center"
	  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('img', {
	    staticClass: "img-responsive",
	    attrs: {
	      "src": "static/img/child.jpg"
	    }
	  }), _vm._v(" "), _c('h2', {
	    staticClass: "title"
	  }, [_vm._v(_vm._s(_vm.data.strings.LEARNING_TABLE_TITLE))]), _vm._v(" "), _c('div', {
	    staticClass: "table-responsive"
	  }, [_c('table', {
	    staticClass: "table"
	  }, [_c('thead', [_c('tr', [_c('th', [_vm._v(_vm._s(_vm.data.strings.LEARNING_FILENAME))]), _vm._v(" "), _c('th', [_vm._v(_vm._s(_vm.data.strings.LEARNING_DIFFICULTY))]), _vm._v(" "), _c('th', [_vm._v(_vm._s(_vm.data.strings.LEARNING_DOWNLOAD))])])]), _vm._v(" "), _c('tbody', _vm._l((_vm.learningSheets), function(item) {
	    return _c('tr', {
	      key: item.title
	    }, [_c('td', [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(item.category))]), _vm._v(" "), _c('td', [_c('a', {
	      attrs: {
	        "href": _vm.processDownloadUrl(item),
	        "target": "blank"
	      },
	      on: {
	        "click": function($event) {
	          _vm.analytics(item.title)
	        }
	      }
	    }, [_c('i', {
	      staticClass: "fa fa-download",
	      attrs: {
	        "aria-hidden": "true"
	      }
	    })])]), _vm._v(" "), _c('td', [_vm._v(_vm._s(item.date))])])
	  }))])]), _vm._v(" "), _c('h2', {
	    staticClass: "title"
	  }, [_vm._v(_vm._s(_vm.data.strings.YOUTUBE_TABLE_TITLE))]), _vm._v(" "), _c('div', {
	    staticClass: "table-responsive"
	  }, [_c('table', {
	    staticClass: "table"
	  }, [_c('thead', [_c('tr', [_c('th', [_vm._v(_vm._s(_vm.data.strings.YOUTUBE_TITLE))])])]), _vm._v(" "), _c('tbody', _vm._l((_vm.data.youtube), function(item) {
	    return _c('tr', {
	      key: item.title
	    }, [_c('td', [_c('a', {
	      attrs: {
	        "href": item.url,
	        "target": "blank"
	      },
	      on: {
	        "click": function($event) {
	          _vm.analytics(item.title)
	        }
	      }
	    }, [_vm._v("\n                " + _vm._s(item.title) + "\n              ")])])])
	  }))])])])])
	},staticRenderFns: []}

/***/ },
/* 171 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "modal fade",
	    attrs: {
	      "id": "myModal",
	      "tabindex": "-1",
	      "role": "dialog",
	      "aria-labelledby": "myModalLabel"
	    }
	  }, [_c('div', {
	    staticClass: "modal-dialog",
	    attrs: {
	      "role": "document"
	    }
	  }, [_c('div', {
	    staticClass: "modal-content"
	  }, [_c('div', {
	    staticClass: "modal-header"
	  }, [_vm._m(0), _vm._v(" "), _c('h4', {
	    staticClass: "modal-title",
	    attrs: {
	      "id": "myModalLabel"
	    }
	  }, [_vm._v(_vm._s(_vm.content.title))])]), _vm._v(" "), (_vm.content.image != null) ? _c('img', {
	    staticClass: "modal-img",
	    attrs: {
	      "src": _vm.content.image
	    }
	  }) : _vm._e(), _vm._v(" "), (_vm.content.video != null) ? _c('iframe', {
	    staticClass: "normal-element",
	    attrs: {
	      "width": "560",
	      "height": "315",
	      "src": _vm.content.video,
	      "frameborder": "0",
	      "allowfullscreen": ""
	    }
	  }) : _vm._e(), _vm._v(" "), _c('div', {
	    staticClass: "modal-body"
	  }, [_vm._v("\n        " + _vm._s(_vm.content.description) + "\n      ")]), _vm._v(" "), (_vm.hasVoiceGuide) ? _c('div', {
	    staticClass: "voice-guide-button",
	    on: {
	      "click": _vm.playVoiceGuide
	    }
	  }, [_vm._v("\n        " + _vm._s(_vm.data.strings.VOICE_GUIDE) + "\n      ")]) : _vm._e(), _vm._v(" "), _c('div', {
	    staticClass: "modal-footer"
	  }, [_c('button', {
	    staticClass: "btn btn-default",
	    attrs: {
	      "type": "button",
	      "data-dismiss": "modal"
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.MODAL_CLOSE))])])], 1)])])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('button', {
	    staticClass: "close",
	    attrs: {
	      "type": "button",
	      "data-dismiss": "modal",
	      "aria-label": "Close"
	    }
	  }, [_c('span', {
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }, [_vm._v("×")])])
	}]}

/***/ },
/* 172 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-md-8 col-md-offset-2"
	  }, [_c('h2', {
	    staticClass: "section-head"
	  }, [_vm._v(_vm._s(_vm.data.title))]), _vm._v(" "), _c('p', [_vm._v("Our app is available on any mobile device! Download now to get started " + _vm._s(_vm.hello))])])])
	},staticRenderFns: []}

/***/ },
/* 173 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return (!_vm.playMode) ? _c('div', {
	    staticClass: "row bg-img main-row fullwidth-row fullheight",
	    attrs: {
	      "play-mode": _vm.playMode
	    }
	  }, [_c('div', {
	    staticClass: "col-xs-12 col-centered fullwidth-col fullheight"
	  }, [_c('div', {
	    staticClass: "row fullwidth-row page-title-row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 fullwidth-col normal-element"
	  }, [_c('h1', {
	    staticClass: "txt text-center page-title pre-wrap"
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_TITLE))])])]), _vm._v(" "), _c('div', {
	    staticClass: "vg-root voice-panel col-centered"
	  }, [_c('div', {
	    staticClass: "row fullwidth-row tip-row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 fullwidth-col"
	  }, [_c('h3', {
	    staticClass: "txt text-center tip flex-vcenter"
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_KEY_TIP_TXT))]), _vm._v(" "), (_vm.isShowError) ? _c('div', {
	    staticClass: "errorMessage text-center"
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_KEY_ERRORMSG_TXT))]) : _vm._e()])]), _vm._v(" "), _c('div', {
	    staticClass: "row edit fullwidth-row"
	  }, [_c('div', {
	    staticClass: "col fullwidth-col"
	  }, [_c('div', {
	    staticClass: "text-center"
	  }, [_vm._v(_vm._s(_vm.voiceGuideID))])])]), _vm._v(" "), _c('div', {
	    staticClass: "row fullwidth-row keyboard-row"
	  }, [_c('div', {
	    staticClass: "col fullwidth-col fullheight"
	  }, [_vm._l((3), function(n) {
	    return _c('div', {
	      staticClass: "row fullwidth-row"
	    }, _vm._l((3), function(k) {
	      return _c('div', {
	        staticClass: "button col-xs-4 col-md-4 col-4 btn-primary flex-vcenter",
	        on: {
	          "click": function($event) {
	            _vm.press(n * 3 + k - 3)
	          }
	        }
	      }, [_vm._v("\n                  " + _vm._s(n * 3 + k - 3) + "\n            ")])
	    }))
	  }), _vm._v(" "), _c('div', {
	    staticClass: "row fullwidth-row"
	  }, [_c('div', {
	    staticClass: "button col-xs-4 col-md-4 col-4 btn-primary disable-btn flex-vcenter"
	  }), _vm._v(" "), _c('div', {
	    staticClass: "button col-xs-4 col-md-4 col-4 btn-primary flex-vcenter",
	    on: {
	      "click": function($event) {
	        _vm.press(0)
	      }
	    }
	  }, [_vm._v("0")]), _vm._v(" "), _c('div', {
	    staticClass: "button col-xs-4 col-md-4 col-4 btn-primary flex-vcenter",
	    on: {
	      "click": function($event) {
	        _vm.press('-')
	      }
	    }
	  }, [_vm._v("-")])]), _vm._v(" "), _c('div', {
	    staticClass: "row fullwidth-row"
	  }, [_c('div', {
	    staticClass: "button button-reverse col-xs-4 col-md-4 col-4 btn-primary flex-vcenter",
	    on: {
	      "click": _vm.clear
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_KEY_CLEAR_BTN))]), _vm._v(" "), _c('div', {
	    staticClass: "button col-xs-4 col-md-4 col-4 btn-primary disable-btn flex-vcenter"
	  }), _vm._v(" "), _c('div', {
	    staticClass: "button button-reverse col-xs-4 col-md-4 col-4 btn-primary flex-vcenter",
	    on: {
	      "click": _vm.confirm
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_KEY_CONFIRM_BTN))])])], 2)])]), _vm._v(" "), _c('div', {
	    staticClass: "row fullwidth-row back-row"
	  }, [_c('div', {
	    staticClass: "col-xs-4 col-centered text-center fullwidth-col"
	  }, [_c('a', {
	    staticClass: "button-reverse back btn btn-secondary",
	    on: {
	      "click": _vm.back
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_KEY_BACK_BTN))])])])])]) : _c('div', {
	    staticClass: "row bg-img main-row fullwidth-row fullheight",
	    attrs: {
	      "play-mode": _vm.playMode
	    }
	  }, [_c('div', {
	    staticClass: "col-xs-12 col-sm-5 col-centered fullwidth-col fullheight"
	  }, [_c('div', {
	    staticClass: "row page-title-row fullwidth-row"
	  }, [_c('div', {
	    staticClass: "col-12 fullwidth-col normal-element"
	  }, [_c('h3', {
	    staticClass: "txt text-center page-title pre-wrap"
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_TITLE))])])]), _vm._v(" "), (_vm.targetVoiceGuideTrack) ? _c('div', {
	    staticClass: "row voice-panel fullwidth-row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 col-centered vg-root fullwidth-col"
	  }, [_c('h3', {
	    staticClass: "txt text-center title"
	  }, [_vm._v(_vm._s(_vm.targetVoiceGuideTrack.title))]), _vm._v(" "), _c('div', {
	    staticClass: "txt text-center index"
	  }, [_vm._v(_vm._s(_vm.targetVoiceGuideTrack.id))]), _vm._v(" "), _c('div', {
	    staticClass: "txt description"
	  }, [_vm._v(_vm._s(_vm.targetVoiceGuideTrack.description))]), _vm._v(" "), _c('audio', {
	    ref: "audioPlayer",
	    attrs: {
	      "id": "vg_audio"
	    }
	  }, [_c('source', {
	    attrs: {
	      "src": _vm.targetVoicePath()
	    }
	  }), _vm._v("\n          Your browser does not support the audio element.\n        ")]), _vm._v(" "), _c('button', {
	    staticClass: "text-center",
	    class: {
	      play: !_vm.isPlay
	    },
	    on: {
	      "click": function($event) {
	        _vm.togglePlay()
	      }
	    }
	  }), _vm._v(" "), _c('div', {
	    staticClass: "progress"
	  }, [_c('div', {
	    staticClass: "progress-bar",
	    style: ({
	      width: _vm.audioProgress + '%'
	    }),
	    attrs: {
	      "role": "progressbar",
	      "aria-valuenow": "25",
	      "aria-valuemin": "0",
	      "aria-valuemax": "100"
	    }
	  })])])]) : _vm._e(), _vm._v(" "), _c('div', {
	    staticClass: "row btn-row fullwidth-row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 col-centered fullwidth-col"
	  }, [_c('a', {
	    staticClass: "button-reverse back btn btn-secondary pull-left normal-element",
	    attrs: {
	      "href": "#home"
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.VOICE_PLAY_HOME_BTN))]), _vm._v(" "), _c('div', {
	    staticClass: "button-reverse back btn btn-secondary pull-right",
	    on: {
	      "click": _vm.back
	    }
	  }, [_vm._v(_vm._s(this.exitText))])])])])])
	},staticRenderFns: []}

/***/ },
/* 174 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row banner fullwidth-row",
	    attrs: {
	      "id": "home"
	    }
	  }, [_c('div', {
	    staticClass: "col-xs-12 flex-vcenter bg-color fullwidth-col"
	  }, [_c('div', {
	    staticClass: "banner"
	  }, [_vm._m(0), _vm._v(" "), _c('div', {
	    staticClass: "row fullwidth-row txt-row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 banner-text fullwidth-col text-center"
	  }, [_c('h1', {
	    staticClass: "white"
	  }, [_vm._v(_vm._s(_vm.data.strings.HOME_TITLE1))]), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.data.strings.HOME_TITLE2))]), _vm._v(" "), _c('a', {
	    staticClass: "btn btn-appoint normal-element",
	    attrs: {
	      "href": "#area"
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.HOME_ACTION_TITLE))])])]), _vm._v(" "), _c('div', {
	    staticClass: "row fullwidth-row app-row normal-element"
	  }, [_c('div', {
	    staticClass: "col-xs-12 app-link text-center"
	  }, [_c('a', {
	    attrs: {
	      "href": _vm.data.source.APP_ANDROID_URL
	    },
	    on: {
	      "click": function($event) {
	        _vm.analytics('Android')
	      }
	    }
	  }, [_c('img', {
	    attrs: {
	      "src": "static/img/google-play-badge.svg",
	      "alt": "Google Play",
	      "height": "35"
	    }
	  })]), _vm._v(" "), _c('a', {
	    attrs: {
	      "href": _vm.data.source.APP_IOS_URL
	    },
	    on: {
	      "click": function($event) {
	        _vm.analytics('iOS')
	      }
	    }
	  }, [_c('img', {
	    attrs: {
	      "src": "static/img/app-store-badge.svg",
	      "alt": "Apple Store",
	      "height": "35"
	    }
	  })]), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.data.strings.HOME_APP_BTN_TIP))])])])])])])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row fullwidth-row logo-row"
	  }, [_c('div', {
	    staticClass: "banner-logo text-center col-xs-12 col-centered"
	  }, [_c('img', {
	    staticClass: "img-responsive",
	    attrs: {
	      "src": "static/img/logo.png"
	    }
	  })])])
	}]}

/***/ },
/* 175 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row",
	    attrs: {
	      "id": "service"
	    }
	  }, [_c('div', {
	    staticClass: "col-xs-10 col-centered"
	  }, [_c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-md-2 col-sm-2 section-title"
	  }, [_c('h1', {
	    staticClass: "ser-title text-nowrap"
	  }, [_vm._v(_vm._s(_vm.title))])]), _vm._v(" "), _c('div', {
	    staticClass: "col-md-5 col-sm-5 text-center"
	  }, [_c('div', {
	    staticClass: "service-info"
	  }, [_vm._m(0), _vm._v(" "), _c('div', {
	    staticClass: "icon-info"
	  }, [_c('h4', [_vm._v(_vm._s(_vm.data.strings.ABOUT_POS_TITLE))]), _vm._v(" "), _c('p', {
	    staticClass: "pre-wrap"
	  }, [_vm._v(_vm._s(_vm.data.strings.ABOUT_POS_TXT))])])])]), _vm._v(" "), _c('div', {
	    staticClass: "col-md-5 col-sm-5 text-center"
	  }, [_c('div', {
	    staticClass: "service-info"
	  }, [_vm._m(1), _vm._v(" "), _c('div', {
	    staticClass: "icon-info"
	  }, [_c('h4', [_vm._v(_vm._s(_vm.data.strings.ABOUT_TIME_TITLE))]), _vm._v(" "), _c('p', {
	    staticClass: "pre-wrap"
	  }, [_vm._v(_vm._s(_vm.data.strings.ABOUT_TIME_TXT))])])])])])])])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "icon"
	  }, [_c('i', {
	    staticClass: "fa fa-map-marker"
	  })])
	},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "icon"
	  }, [_c('i', {
	    staticClass: "fa fa-clock-o"
	  })])
	}]}

/***/ },
/* 176 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary",
	    attrs: {
	      "id": "main_row"
	    }
	  }, [_c('div', {
	    staticClass: "col-12"
	  }, [_vm._t("default")], 2)])
	},staticRenderFns: []}

/***/ },
/* 177 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary",
	    attrs: {
	      "desktop-mode": _vm.desktopMode
	    }
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('div', {
	    staticClass: "carousel slide",
	    attrs: {
	      "id": "gameCarousel",
	      "data-ride": "carousel"
	    }
	  }, [_c('h1', {
	    staticClass: "block_text normal-element text-center"
	  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('div', {
	    staticClass: "carousel-inner",
	    attrs: {
	      "role": "listbox"
	    }
	  }, [_vm._l((_vm.data.game), function(item, index) {
	    return _c('div', {
	      class: ['item', {
	        'active': index === 0
	      }]
	    }, [_c('div', {
	      staticClass: "carousel-image app-element img-responsive",
	      style: ({
	        backgroundImage: 'url(' + item.image + ')'
	      })
	    }), _vm._v(" "), _c('h2', {
	      staticClass: "area-title app-element"
	    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('img', {
	      staticClass: "normal-element img-responsive",
	      attrs: {
	        "src": item.image,
	        "alt": "area-photo"
	      }
	    }), _vm._v(" "), _c('div', {
	      staticClass: "text_block carousel-caption normal-element"
	    }, [_c('h2', {
	      staticClass: "area-title normal-element"
	    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), (_vm.desktopMode) ? _c('div', {
	      staticClass: "text-left description"
	    }, [_vm._v(_vm._s(item.description))]) : _vm._e()])])
	  }), _vm._v(" "), (!_vm.appMode) ? _c('div', {
	    staticClass: "control"
	  }, [_c('a', {
	    staticClass: "left carousel-control",
	    attrs: {
	      "href": "#gameCarousel",
	      "role": "button",
	      "data-slide": "prev"
	    },
	    on: {
	      "click": function($event) {
	        _vm.prev()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-left",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Previous")])]), _vm._v(" "), _c('a', {
	    staticClass: "right carousel-control",
	    attrs: {
	      "href": "#gameCarousel",
	      "role": "button",
	      "data-slide": "next"
	    },
	    on: {
	      "click": function($event) {
	        _vm.next()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-right",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Next")])])]) : _vm._e(), _vm._v(" "), _c('ol', {
	    staticClass: "carousel-indicators"
	  }, _vm._l((_vm.data.game), function(item, index) {
	    return _c('li', {
	      class: {
	        active: index === 0
	      },
	      attrs: {
	        "data-slide-to": index,
	        "data-target": "#gameCarousel"
	      },
	      on: {
	        "click": function($event) {
	          _vm.choose(index)
	        }
	      }
	    })
	  }))], 2)]), _vm._v(" "), (!_vm.desktopMode) ? _c('div', {
	    staticClass: "description"
	  }, [_vm._v(_vm._s(_vm.data.game[_vm.current_idx].description))]) : _vm._e(), _vm._v(" "), (_vm.appMode) ? _c('div', {
	    staticClass: "control"
	  }, [_c('a', {
	    staticClass: "left carousel-control",
	    attrs: {
	      "href": "#gameCarousel",
	      "role": "button",
	      "data-slide": "prev"
	    },
	    on: {
	      "click": function($event) {
	        _vm.prev()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-left",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Previous")])]), _vm._v(" "), _c('a', {
	    staticClass: "right carousel-control",
	    attrs: {
	      "href": "#gameCarousel",
	      "role": "button",
	      "data-slide": "next"
	    },
	    on: {
	      "click": function($event) {
	        _vm.next()
	      }
	    }
	  }, [_c('span', {
	    staticClass: "fa fa-chevron-right",
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }), _vm._v(" "), _c('span', {
	    staticClass: "sr-only"
	  }, [_vm._v("Next")])])]) : _vm._e()])])
	},staticRenderFns: []}

/***/ },
/* 178 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    ref: "page-container",
	    staticClass: "container bg-primary"
	  }, [(!_vm.loggedIn) ? _c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-xs-12 col-md-offset-2 col-md-8"
	  }, [_c('div', {
	    staticClass: "vg_root container-fluid"
	  }, [_c('div', {
	    staticClass: "row"
	  }, [_c('p', {
	    staticClass: "tip"
	  }, [_vm._v("\n            " + _vm._s(_vm.data.strings.APPLOGIN_TIP) + "\n          ")])]), _vm._v(" "), _c('div', {
	    staticClass: "row"
	  }, [_c('h4', {
	    staticClass: "col-xs-12 col-12"
	  }, [_vm._v(_vm._s(_vm.data.strings.APPLOGIN_NAME)), _c('span', {
	    staticClass: "required"
	  }, [_vm._v("*")])]), _vm._v(" "), _c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.name),
	      expression: "name"
	    }],
	    staticClass: "edit col-xs-12 col-12",
	    attrs: {
	      "maxlength": "32"
	    },
	    domProps: {
	      "value": (_vm.name)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) { return; }
	        _vm.name = $event.target.value
	      }
	    }
	  })]), _vm._v(" "), _c('div', {
	    staticClass: "row"
	  }, [_c('h4', {
	    staticClass: "col-xs-12 col-12"
	  }, [_vm._v(_vm._s(_vm.data.strings.APPLOGIN_EMAIL)), _c('span', {
	    staticClass: "required"
	  }, [_vm._v("*")])]), _vm._v(" "), _c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.email),
	      expression: "email"
	    }],
	    staticClass: "edit col-xs-12 col-12",
	    attrs: {
	      "maxlength": "64",
	      "type": "email"
	    },
	    domProps: {
	      "value": (_vm.email)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) { return; }
	        _vm.email = $event.target.value
	      }
	    }
	  })]), _vm._v(" "), _c('div', {
	    staticClass: "row"
	  }, [_c('h4', {
	    staticClass: "col-xs-12 col-12"
	  }, [_vm._v(_vm._s(_vm.data.strings.APPLOGIN_GENDER)), _c('span', {
	    staticClass: "required"
	  }, [_vm._v("*")])]), _vm._v(" "), _c('div', [_c('label', {
	    staticClass: "radio-inline"
	  }, [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.gender),
	      expression: "gender"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "M"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.gender, "M")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.gender = "M"
	      }
	    }
	  }), _vm._v(_vm._s(_vm.data.strings.APPLOGIN_MALE))]), _vm._v(" "), _c('label', {
	    staticClass: "radio-inline"
	  }, [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.gender),
	      expression: "gender"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "F"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.gender, "F")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.gender = "F"
	      }
	    }
	  }), _vm._v(_vm._s(_vm.data.strings.APPLOGIN_FEMALE))])])]), _vm._v(" "), _c('div', {
	    staticClass: "row"
	  }, [_c('h4', {
	    staticClass: "col-xs-12 col-12"
	  }, [_vm._v(_vm._s(_vm.data.strings.APPLOGIN_AGE)), _c('span', {
	    staticClass: "required"
	  }, [_vm._v("*")])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "12"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "12")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "12"
	      }
	    }
	  }), _vm._v(_vm._s(_vm.data.strings.APPLOGIN_UNDER12))])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "18"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "18")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "18"
	      }
	    }
	  }), _vm._v("13~18")])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "24"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "24")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "24"
	      }
	    }
	  }), _vm._v("19~24")])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "30"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "30")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "30"
	      }
	    }
	  }), _vm._v("24~30")])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "40"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "40")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "40"
	      }
	    }
	  }), _vm._v("31~40")])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "60"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "60")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "60"
	      }
	    }
	  }), _vm._v("41~60")])]), _vm._v(" "), _c('div', {
	    staticClass: "col-xs-6 col-6 radio"
	  }, [_c('label', [_c('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (_vm.age),
	      expression: "age"
	    }],
	    attrs: {
	      "type": "radio",
	      "value": "70"
	    },
	    domProps: {
	      "checked": _vm._q(_vm.age, "70")
	    },
	    on: {
	      "__c": function($event) {
	        _vm.age = "70"
	      }
	    }
	  }), _vm._v(_vm._s(_vm.data.strings.APPLOGIN_OLDER60))])])])])])]) : _vm._e(), _vm._v(" "), _c('div', {
	    directives: [{
	      name: "show",
	      rawName: "v-show",
	      value: (_vm.loggedIn),
	      expression: "loggedIn"
	    }],
	    ref: "qr-code-container",
	    staticClass: "qr-code-container"
	  }), _vm._v(" "), _c('div', {
	    staticClass: "row justify-content-end"
	  }, [(_vm.loggedIn) ? _c('p', {
	    staticClass: "col-md-12"
	  }, [_vm._v("\n      " + _vm._s(_vm.data.strings.APPLOGIN_TIP2) + "\n    ")]) : _vm._e(), _vm._v(" "), (!_vm.loggedIn) ? _c('div', {
	    staticClass: "button col-xs-6 col-xs-offset-3 btn-primary",
	    on: {
	      "click": _vm.login
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.APPLOGIN_REGISTER))]) : _vm._e(), _vm._v(" "), (_vm.loggedIn) ? _c('div', {
	    staticClass: "logout-btn button col-xs-6 col-xs-offset-3 btn-primary",
	    on: {
	      "click": _vm.logout
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.APPLOGIN_RE_REGISTER))]) : _vm._e()])])
	},staticRenderFns: []}

/***/ },
/* 179 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('nav', {
	    staticClass: "navbar navbar-default navbar-fixed-top"
	  }, [_c('div', {
	    staticClass: "row justify-content-between"
	  }, [_vm._m(0), _vm._v(" "), _c('div', {
	    staticClass: "col-sm-10 col-xs-12"
	  }, [_c('div', {
	    staticClass: "collapse navbar-collapse navbar-right",
	    attrs: {
	      "id": "myNavbar"
	    }
	  }, [_c('ul', {
	    staticClass: "nav navbar-nav"
	  }, [_vm._l((_vm.menus), function(item) {
	    return _c('li', {}, [_c('a', {
	      attrs: {
	        "href": item.link
	      },
	      on: {
	        "click": function($event) {
	          _vm.analytics(item.link)
	        }
	      }
	    }, [_vm._v(_vm._s(item.title))])])
	  }), _vm._v(" "), _c('li', {
	    on: {
	      "click": function($event) {
	        _vm.toggleLocale()
	      }
	    }
	  }, [_c('a', [_vm._v(_vm._s(_vm.data.strings.TOGGLE_LOCALE))])])], 2)])])])])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "col-sm-2 col-xs-12"
	  }, [_c('div', {
	    staticClass: "navbar-header"
	  }, [_c('button', {
	    staticClass: "navbar-toggle",
	    attrs: {
	      "type": "button",
	      "data-toggle": "collapse",
	      "data-target": "#myNavbar"
	    }
	  }, [_c('span', {
	    staticClass: "icon-bar"
	  }), _vm._v(" "), _c('span', {
	    staticClass: "icon-bar"
	  }), _vm._v(" "), _c('span', {
	    staticClass: "icon-bar"
	  })]), _vm._v(" "), _c('a', {
	    staticClass: "navbar-brand",
	    attrs: {
	      "href": "#"
	    }
	  }, [_c('img', {
	    staticClass: "img-responsive",
	    attrs: {
	      "src": "static/img/logo_eng.png"
	    }
	  })])])])
	}]}

/***/ },
/* 180 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    attrs: {
	      "id": "app",
	      "app-mode": _vm.appMode
	    }
	  }, [
	    [_c('div', {
	      staticClass: "container-fluid"
	    }, [(_vm.isHeaderVisible) ? _c('HeaderPart') : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['applogin']) ? _c('AppLogin', {
	      attrs: {
	        "data": _vm.data
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['aboutapp']) ? _c('AboutApp', {
	      attrs: {
	        "id": "aboutapp",
	        "data": _vm.data
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['home']) ? _c('HomeBlock') : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['area']) ? _c('Exhibitions', {
	      attrs: {
	        "id": "area",
	        "data": _vm.data,
	        "title": _vm.getTitle('area'),
	        "app-mode": _vm.appMode
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['game']) ? _c('GameBlock', {
	      attrs: {
	        "id": "game",
	        "data": _vm.data,
	        "title": _vm.getTitle('game'),
	        "app-mode": _vm.appMode
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['voiceguide']) ? _c('VoiceGuide', {
	      attrs: {
	        "id": "voiceguide",
	        "data": _vm.data,
	        "defaultVoiceGuideID": _vm.defaultVoiceGuideID
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['learn']) ? _c('LearningBlock', {
	      attrs: {
	        "id": "learn",
	        "data": _vm.data,
	        "title": _vm.getTitle('learn')
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['quiz']) ? _c('QuizBlock', {
	      attrs: {
	        "id": "quiz",
	        "data": _vm.data,
	        "title": _vm.getTitle('quiz')
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['map']) ? _c('MapBlock', {
	      attrs: {
	        "id": "map",
	        "data": _vm.data,
	        "title": _vm.getTitle('map')
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['transportation']) ? _c('TrafficsBlock', {
	      attrs: {
	        "id": "transportation",
	        "data": _vm.data,
	        "title": _vm.getTitle('transportation')
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isBlockVisible['about']) ? _c('AboutBlock', {
	      attrs: {
	        "id": "about",
	        "data": _vm.data,
	        "title": _vm.getTitle('about')
	      }
	    }) : _vm._e(), _vm._v(" "), (_vm.isFooterVisible) ? _c('FooterPart', {
	      attrs: {
	        "data": _vm.data
	      }
	    }) : _vm._e(), _vm._v(" "), _c('ModalTemplate', {
	      attrs: {
	        "data": _vm.data
	      }
	    })], 1)]
	  ], 2)
	},staticRenderFns: []}

/***/ },
/* 181 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row bg-primary"
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-lg-9 col-centered"
	  }, [_c('h2', {
	    staticClass: "title"
	  }, [_vm._v(_vm._s(_vm.data.strings.ABOUT_APP_USER_TERMS_TITLE))]), _vm._v(" "), _c('div', {
	    staticClass: "user-terms"
	  }, [_vm._v(_vm._s(_vm.data.strings.ABOUT_APP_USER_TERMS))])])])
	},staticRenderFns: []}

/***/ },
/* 182 */
/***/ function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-xs-11 col-centered"
	  }, [_c('footer', {
	    attrs: {
	      "id": "footer"
	    }
	  }, [_c('div', {
	    staticClass: "top-footer"
	  }, [_c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-md-4 col-sm-4 col-md-offset-1 marb20"
	  }, [_c('div', {
	    staticClass: "info-sec"
	  }, [_c('img', {
	    attrs: {
	      "src": "static/img/nmns_logo.png"
	    }
	  }), _vm._v(" "), _c('br'), _c('br'), _vm._v(" "), _c('p', {
	    staticClass: "pre-wrap"
	  }, [_vm._v(_vm._s(_vm.data.strings.FOOTER_DESCRIPTION) + "\n"), _c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_PRIVACY_URL
	    }
	  }, [_vm._v(_vm._s(_vm.data.strings.FOOTER_PRIVACY))])])])]), _vm._v(" "), _c('div', {
	    staticClass: "col-md-3 col-sm-3 marb20"
	  }, [_c('div', {
	    staticClass: "ftr-tle"
	  }, [_c('h4', {
	    staticClass: "white no-padding"
	  }, [_vm._v(_vm._s(_vm.data.strings.FOOTER_REF_TITLE))])]), _vm._v(" "), _c('div', {
	    staticClass: "info-sec"
	  }, [_c('ul', {
	    staticClass: "quick-info"
	  }, [_c('li', [_c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_HOMEPAGE_URL
	    }
	  }, [_c('i', {
	    staticClass: "fa fa-link"
	  }), _vm._v(_vm._s(_vm.data.strings.FOOTER_REF_LINK1))])]), _vm._v(" "), _c('li', [_c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_IG_URL
	    }
	  }, [_c('i', {
	    staticClass: "fa fa-link"
	  }), _vm._v(_vm._s(_vm.data.strings.FOOTER_REF_LINK2))])]), _vm._v(" "), _c('li', [_c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_U2_URL
	    }
	  }, [_c('i', {
	    staticClass: "fa fa-link"
	  }), _vm._v(_vm._s(_vm.data.strings.FOOTER_REF_LINK3))])]), _vm._v(" "), _c('li', [_c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_FB_URL
	    }
	  }, [_c('i', {
	    staticClass: "fa fa-link"
	  }), _vm._v(_vm._s(_vm.data.strings.FOOTER_REF_LINK4))])])])])]), _vm._v(" "), _c('div', {
	    staticClass: "col-md-3 col-sm-3 marb20 col-md-offset-1"
	  }, [_c('div', {
	    staticClass: "ftr-tle"
	  }, [_c('h4', {
	    staticClass: "white no-padding"
	  }, [_vm._v(_vm._s(_vm.data.strings.FOOTER_FOLLOW_TITLE))])]), _vm._v(" "), _c('div', {
	    staticClass: "info-sec"
	  }, [_c('ul', {
	    staticClass: "social-icon"
	  }, [_c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_FB_URL,
	      "target": "_blank"
	    }
	  }, [_vm._m(0)]), _vm._v(" "), _c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_IG_URL,
	      "target": "_blank"
	    }
	  }, [_vm._m(1)]), _vm._v(" "), _c('a', {
	    attrs: {
	      "href": _vm.data.source.FOOTER_REF_U2_URL,
	      "target": "_blank"
	    }
	  }, [_vm._m(2)])])])])])]), _vm._v(" "), _c('div', {
	    staticClass: "footer-line"
	  }, [_c('div', {
	    staticClass: "row"
	  }, [_c('div', {
	    staticClass: "col-md-12 text-center"
	  }, [_vm._v(_vm._s(_vm.data.strings.FOOTER_COPYRIGHT_ENG))]), _vm._v(" "), _c('div', {
	    staticClass: "col-md-12 text-center"
	  }, [_vm._v(_vm._s(_vm.data.strings.FOOTER_COPYRIGHT_ZH))]), _vm._v(" "), _c('br'), _c('br')])])])])])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('li', {
	    staticClass: "bgred"
	  }, [_c('i', {
	    staticClass: "fa fa-facebook"
	  })])
	},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('li', {
	    staticClass: "bgdark-blue"
	  }, [_c('i', {
	    staticClass: "fa fa-instagram"
	  })])
	},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('li', {
	    staticClass: "bglight-blue"
	  }, [_c('i', {
	    staticClass: "fa fa-youtube"
	  })])
	}]}

/***/ }
]);
//# sourceMappingURL=app.c3abb2fefb56ed2e79df.js.map