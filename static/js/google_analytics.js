module.exports = {
  init(id, isApp) {
    window.ga = window.ga || function () { (ga.q = ga.q || []).push(arguments); }; ga.l = +new Date();
    if (isApp) {
      let uuid = localStorage.getItem('uuid');
      if (!uuid) {
        uuid = this.generateUUID();
        localStorage.setItem('uuid', uuid);
      }

  		ga('create', id, {
        'cookieDomain': 'none',
        'storage': 'none',
        'clientId': uuid
      });

      ga('set', 'checkProtocolTask', () => {});
    } else {
      ga('create', id, 'auto');
    }
    ga('send', 'pageview');
  },

  generateUUID() {
    let d = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }
};
